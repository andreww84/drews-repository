﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoveSix
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number");
            var num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Now enter a second number");
            var num2 = int.Parse(Console.ReadLine());
            var result = LoveSix(num1, num2);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        public static bool LoveSix(int a, int b)
        {
            if (a == 6 || b == 6)
            {
                return true;
            }
            else if (a - b == 6 || a + b == 6)
            {
                return true;
            }
            return false;
        }

//The number 6 is a truly great number. Given two int values, a and b, return true if either one is 6. Or if their sum or difference is 6.

//LoveSix(6, 4) → true
//LoveSix(4, 5) → false
//LoveSix(1, 5) → true

//public bool LoveSix(int a, int b) {
  
//}

    }
}
