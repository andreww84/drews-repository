﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabAddressBook
{
    class Address
    {
        private string name;
        private string streetAddress;
        private string cityName;
        private string state;
        private string zip;
        private PhoneNumber[] phoneNumbers = new PhoneNumber[1024];
        private string emailAddress;
        private DateTime birthday;

        public void AddPhoneNumber(string number, string type)
        {
            for (int i = 0; i < phoneNumbers.Length; i++)
            {
                if (phoneNumbers[i] = null)
                {
                    phoneNumbers[i] = new PhoneNumber();
                    phoneNumbers[i].SetNumber(number);
                    phoneNumbers[i].SetType(type);
                    break;
                }
            }
        }

        public void Name(string last, string first)
        {
            name = last + ", " + first;
        }

        public void StreetAddr(int number, string street)
        {
            streetAddress = number + "" + street;
        }

        public void City(string city)
        {
            cityName = city;
        }

        public void ZipCode(string zipNum)
        {
            zip = zipNum;
        }

        public void Phone(string[] phoneNum)
        {
            phoneNumbers = phoneNum;
        }

        public void Email(string email)
        {
            emailAddress = email;
        }
    }
}
