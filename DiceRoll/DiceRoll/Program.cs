﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiceRoll
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] dice = new int[6];
            Random rng = new Random();

            Console.WriteLine("Press any key to continue");
            Console.ReadLine();

            for (int i = 0; i < 100; i++)
            {
                int diceRoll = 0;
                diceRoll = rng.Next(6);
                dice[diceRoll]++;
                Console.WriteLine("You rolled the dice");
                Console.ReadLine();
            }


        }
    }
}
