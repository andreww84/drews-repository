﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BF4QuickLoadout.Models
{
    public class Shotgun : Weapon
    {
        public int Pellets { get; private set; }
        public bool IsBuckshot { get; private set; }
        public bool IsFlechette { get; private set; }
        public bool IsFrag { get; private set; }
        public bool IsSlug { get; private set; }

        public Shotgun(int id, string type, decimal min, decimal max, decimal dropStart, decimal dropEnd, int fireRate, int burstRate,
            int muzVel, int muzVelSup, string bulDrop, string cap, string proj, decimal relSpd, decimal relSpdLeft, decimal recUp,
            decimal recLeft, decimal recRight, decimal firstShot, decimal hip, int pellets, bool isBuck, bool isFlech, bool isFrag, bool isSlug): base(id, type, min, max, dropStart, dropEnd, fireRate, burstRate,
              muzVel, muzVelSup, bulDrop, cap, proj, relSpd, relSpdLeft, recUp, recLeft, recRight, firstShot, hip)
        {
            Pellets = pellets;
            IsBuckshot = isBuck;
            IsFlechette = isFlech;
            IsFrag = isFrag;
            IsSlug = isSlug;
        }
    }
}