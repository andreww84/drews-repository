﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BF4QuickLoadout.Models
{
    public class Attachment
    {
        public int Id { get; private set; }
        public string AttachmentType { get; private set; }
        public decimal FirstShotRecoilFX { get; private set; }
        public decimal RecoilUpFX { get; private set; }
        public decimal RecoilLeftFX { get; private set; }
        public decimal RecoilRightFX { get; private set; }
        public decimal HipFireFX { get; private set; }
    }
}