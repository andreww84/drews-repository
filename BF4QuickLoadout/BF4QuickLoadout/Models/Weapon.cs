﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BF4QuickLoadout.Models
{
    public class Weapon
    {
        public int WeaponId { get; private set; }
        public string WeaponType { get; private set; }
        public decimal MinimumDamage { get; private set; }
        public decimal MaximumDamage { get; private set; }
        public decimal DropOffStart { get; private set; }
        public decimal DropOffEnd { get; private set; }
        public int RateOfFire { get; private set; }
        public int RateOfFireBurst { get; private set; }
        //public int FireModes { get; private set; }
        public int MuzzleVelocity { get; private set; }
        public int MuzzleVelocitySuppressed { get; private set; }
        public string BulletDrop { get; private set; }
        public string Capacity { get; private set; }
        public string Projectile { get; private set; }
        public decimal ReloadSpeed { get; private set; }
        public decimal ReloadSpeedChamber { get; private set; }
        public decimal RecoilUp { get; private set; }
        public decimal RecoilLeft { get; private set; }
        public decimal RecoilRight { get; private set; }
        public decimal FirstShotRecoilMultiplier { get; private set; }
        public decimal HipFireAvg { get; private set; }


        public Weapon(int id, string type, decimal min, decimal max, decimal dropStart, decimal dropEnd, int fireRate, int burstRate,
            int muzVel, int muzVelSup, string bulDrop, string cap, string proj, decimal relSpd, decimal relSpdLeft, decimal recUp,
            decimal recLeft, decimal recRight, decimal firstShot, decimal hip)
        {
            WeaponId = id;
            WeaponType = type;
            MinimumDamage = min;
            MaximumDamage = max;
            DropOffStart = dropStart;
            DropOffEnd = dropEnd;
            RateOfFire = fireRate;
            RateOfFireBurst = burstRate;
            MuzzleVelocity = muzVel;
            MuzzleVelocitySuppressed = muzVelSup;
            BulletDrop = bulDrop;
            Capacity = cap;
            Projectile = proj;
            ReloadSpeed = relSpd;
            ReloadSpeedChamber = relSpdLeft;
            RecoilUp = recUp;
            RecoilLeft = recLeft;
            RecoilRight = recRight;
            FirstShotRecoilMultiplier = firstShot;
            HipFireAvg = hip;
        }
    }

}