﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontTimes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string");
            var input1 = Console.ReadLine();
            Console.WriteLine("Now enter a non-negative integer");
            var input2 = Console.ReadLine();
            var num = int.Parse(input2);
            var theMethod = FrontTimes(input1, num);
            Console.WriteLine(theMethod);
            Console.ReadLine();
        }

        public static string FrontTimes(string str, int n)
        {
            string newStr = null;
            for (int i = 0; i < n; i++)
            {
                if (str.Length < 3)

                    newStr += str;

                else newStr += str.Substring(0, 3);

            }
            return newStr;


        }

//Given a string and a non-negative int n, we'll say that the front of the string is the first 3 chars, or whatever is there if the string is less than length 3. Return n copies of the front; 

//FrontTimes("Chocolate", 2) -> "ChoCho"
//FrontTimes("Chocolate", 3) -> "ChoChoCho"
//FrontTimes("Abc", 3) -> "AbcAbcAbc"

//public string FrontTimes(string str, int n) {

//}
    }
}
