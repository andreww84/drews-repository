﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using System.Threading.Tasks;

namespace StringTimes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string");
            var input1 = Console.ReadLine();
            Console.WriteLine("Enter a non-negative integer");
            var input2 = Console.ReadLine();
            var num = int.Parse(input2);
            var times = StringTimes(input1, num);
            Console.WriteLine(times);
            Console.ReadLine();
        }

        public static string StringTimes(string str, int n)
        {
            for (int i = 1; i < n; i++)
            {
                Console.WriteLine(str + " there!");
            }
            return str + " there!";
        }

//Given a string and a non-negative int n, return a larger string that is n copies of the original string. 

//StringTimes("Hi", 2) -> "HiHi"
//StringTimes("Hi", 3) -> "HiHiHi"
//StringTimes("Hi", 1) -> "Hi"

//public string StringTimes(string str, int n) {

//}
    }
}
