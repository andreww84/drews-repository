﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakePi
{
    class Program
    {
        static void Main(string[] args)
        {
            var returnPi = MakePi(3);
            for (int i = 0; i < returnPi.Length; i++)
            {
                Console.Write(returnPi[i]);
            }
            Console.ReadLine();
        }

        public static int[] MakePi(int n)
        {
            int[] arr1 = new int[12];
            arr1[0] = 3;
            arr1[1] = 1;
            arr1[2] = 4;
            arr1[3] = 1;
            arr1[4] = 5;
            arr1[5] = 9;
            arr1[6] = 2;
            arr1[7] = 6;
            arr1[8] = 5;
            arr1[9] = 3;
            arr1[10] = 5;
            arr1[11] = 9;
            int[] arr2 = new int[n];

                for (int i = 0; i < n; i++)
                {
                    arr2[i] = arr1[i];
                }

                return arr2;

        }
    }
}
