﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Unlucky1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = {1, 3, 5, 7};
            var result = Unlucky1(arr1);
            Console.Write(result);
            Console.ReadLine();
        }

        public static bool Unlucky1(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
                if ((numbers[0] == 1 || numbers[1] == 1) && (numbers[1] == 3 || numbers[2] == 3))
                {
                    return true;
                }
                else if (numbers.Length - 2 == 1 || numbers.Length - 1 == 1)
                {
                    return true;
                }
            return false;
        }

//We'll say that a 1 immediately followed by a 3 in an array is an "unlucky" 1. 
//Return true if the given array contains an unlucky 1 in the first 2 or last 2 positions in the array. 

//Unlucky1({1, 3, 4, 5}) -> true
//Unlucky1({2, 1, 3, 4, 5}) -> true
//Unlucky1({1, 1, 1}) -> false

//public bool Unlucky1(int[] numbers) {

//}
    }
}
