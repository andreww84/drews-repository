﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HasEven
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new int[2] {2, 5};

            var result = HasEven(arr1);

            Console.WriteLine(result);
            Console.ReadLine();
        }

        public static bool HasEven(int[] numbers)
        {
            foreach (var number in numbers)
            {
                if (number % 2 == 0)
                {
                    return true;
                }
            }
            return false;
        }

//Given an int array , return true if it contains an even number (HINT: Use Mod (%)). 

//HasEven({2, 5}) -> true
//HasEven({4, 3}) -> true
//HasEven({7, 5}) -> false

//public bool HasEven(int[] numbers) {

//}
    }
}
