﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RotateLeft
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new int[3] {1, 2, 3};

            var result = RotateLeft(arr1);

            foreach (var number in result)
            {
                Console.WriteLine(number);
            }
                Console.ReadLine();
        }

        public static int[] RotateLeft(int[] numbers)
        {
            int[] newArr = new int[3];
            for (int i = 0; i < numbers.Length; i++)
            {
                if (i == 0)
                {
                    newArr[0] = numbers[1];
                    newArr[1] = numbers[2];
                    newArr[2] = numbers[i];
                }
            }
            return newArr;
        }

//Given an array of ints, return an array with the elements "rotated left" so {1, 2, 3} yields {2, 3, 1}. 

//RotateLeft({1, 2, 3}) -> {2, 3, 1}
//RotateLeft({5, 11, 9}) -> {11, 9, 5}
//RotateLeft({7, 0, 0}) -> {0, 0, 7}

//public int[] RotateLeft(int[] numbers) {

//}
    }
}
