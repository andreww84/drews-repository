﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace KeepLast
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = {1, 2, 3};

            var result = KeepLast(arr1);

            foreach (var number in result)
            {
                Console.WriteLine(number);
            }
            Console.ReadLine();
        }

        // FOUND ANOTHER EASIER/SHORTER WAY TO DO THIS SAME METHOD (SEE BELOW)

        //public static int[] KeepLast(int[] numbers)
        //{
        //    int[] newArr = new int[numbers.Length * 2];

        //    for (int i = 0; i < numbers.Length; i++)
        //    {
        //        if (i == numbers.Length - 1)
        //        {
        //            newArr[newArr.Length - 1] = numbers[numbers.Length - 1];
        //        }
        //    }
        //    return newArr;
        //}

        public static int[] KeepLast(int[] numbers)
        {
            int[] newArr = new int[numbers.Length * 2];

            for (int i = 0; i < numbers.Length; i++)
            {
                    if (i == numbers.Length - 1)
                    {
                        newArr[newArr.Length - 1] = numbers[numbers.Length - 1];
                    }
            }
            return newArr;
        }
       

//Given an int array, return a new array with double the length where its last element is the same as the original array, and all the other elements are 0. The original array will be length 1 or more. Note: by default, a new int array contains all 0's. 

//KeepLast({4, 5, 6}) -> {0, 0, 0, 0, 0, 6}
//KeepLast({1, 2}) -> {0, 0, 0, 2}
//KeepLast({3}) -> {0, 3}

//public int[] KeepLast(int[] numbers) {

//}
    }
}
