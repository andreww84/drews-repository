﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace FirstLast6
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] arr1 = new int[]
            {
                1, 2, 3, 4, 6
            };
            
            var trueFalse = FirstLast6(arr1);
            Console.WriteLine(trueFalse);
            Console.ReadLine();
        }

        public static bool FirstLast6(int[] numbers)
        {
            if ((numbers[numbers.Length - 1] == 6) || (numbers[0] == 6))
            {
                return true;
            }

            return false;
        }
    }
}
