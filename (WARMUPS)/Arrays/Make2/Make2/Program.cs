﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Make2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = {8, 9};
            int[] arr2 = {5, 5};

            var result = Make2(arr1, arr2);

            foreach (var element in result)
            {
                Console.WriteLine(element);
            }
            Console.ReadLine();
        }

        public static int[] Make2(int[] a, int[] b)
        {
            int[] newArr = new int[2];
            if (a.Length == 0)
            {
                newArr = b;
            }
            else if (a.Length == 1)
            {
                newArr[0] = a[0];
                newArr[1] = b[0];
            }
            else if (a.Length >= 2)
            {
                newArr[0] = a[0];
                newArr[1] = a[1];
            }
            return newArr;
        }
        

//Given 2 int arrays, a and b, return a new array length 2 containing, as much as will fit, the elements from a followed by the elements from b. The arrays may be any length, including 0, but there will be 2 or more elements available between the 2 arrays. 

//Make2({4, 5}, {1, 2, 3}) -> {4, 5}
//Make2({4}, {1, 2, 3}) -> {4, 1}
//Make2({}, {1, 2}) -> {1, 2}

//public int[] make2(int[] a, int[] b) {
  
//}
    }
}
