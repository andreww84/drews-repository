﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetMiddle
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new int[3] {1, 2, 3};
            int[] arr2 = new int[3] { 4, 5, 6 };

            var result = GetMiddle(arr1, arr2);

            // prints out each number in the returned array
            foreach (int number in result)
            {
                Console.WriteLine(number);
            }
            Console.ReadLine();
        }
            // Gets the middle element of each array
        public static int[] GetMiddle(int[] a, int[] b)
        {
            int[] returnedArray = new int[2];

            for (int i = 0; i < a.Length; i++)
            {
                if (i == 1)
                {
                    returnedArray[0] = a[i];
                    returnedArray[1] = b[i];
                }
            }
            return returnedArray;
        }

        //Given 2 int arrays, a and b, each length 3, return a new array length 2 containing their middle elements. 

        //GetMiddle({1, 2, 3}, {4, 5, 6}) -> {2, 5}
        //GetMiddle({7, 7, 7}, {3, 8, 0}) -> {7, 8}
        //GetMiddle({5, 2, 9}, {1, 4, 5}) -> {2, 4}

        //public int[] GetMiddle(int[] a, int[] b) {

        //}
    }
}
