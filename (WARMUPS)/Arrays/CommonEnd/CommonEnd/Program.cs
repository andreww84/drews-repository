﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonEnd
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new int[4];
            arr1[0] = 0;
            arr1[1] = 1;
            arr1[2] = 2;
            arr1[3] = 3;

            int[] arr2 = new int[4];
            arr2[0] = 0;
            arr2[1] = 1;
            arr2[2] = 2;
            arr2[3] = 3;

            var trueOrFalse = CommonEnd(arr1, arr2);
            Console.Write(trueOrFalse);
            Console.ReadLine();
        }

        public static bool CommonEnd(int[] a, int[] b)
        {
            if ((a[0] == b[0]) || (a[a.Length - 1] == b[b.Length - 1]))
            {
                return true;
            }

            return false;
        }

    }
}
