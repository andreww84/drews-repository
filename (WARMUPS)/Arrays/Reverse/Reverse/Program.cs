﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Reverse
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array1 = new int[3] {1, 2, 3};

            int[] result = Reverse(array1);

            foreach (var number in result)
            {
                Console.WriteLine(number);
            }
            Console.ReadLine();
        }

        public static int[] Reverse(int[] numbers)
        {
            int[] newArr = new int[3];
            for (int i = 0; i < numbers.Length; i++)
            {
                newArr[0] = numbers[2];
                newArr[1] = numbers[1];
                newArr[2] = numbers[0];
            }
            return newArr;

        }

//Given an array of ints length 3, return a new array with the elements in reverse order, so for example {1, 2, 3} becomes {3, 2, 1}. 

//public int[] Reverse(int[] numbers) {

//}
    }
}
