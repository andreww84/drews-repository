﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Fix23
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = {1, 2, 3};
            var result = Fix23(arr1);

            foreach (var element in result)
            {
                Console.WriteLine(element);
            }
            Console.ReadLine();
        }

        public static int[] Fix23(int[] numbers)
        {
            int[] newArr = new int[3];
            for (int i = 0; i < numbers.Length; i++)
            {
                if ((numbers[0] == 2 && numbers[1] == 3) || (numbers[1] == 2 && numbers[2] == 3))
                {
                    newArr[0] = numbers[0];
                    newArr[1] = numbers[1];
                    newArr[2] = 0;
                }   
            }
            return newArr;
        }

//Given an int array length 3, if there is a 2 in the array immediately followed by a 3, set the 3 element to 0. Return the changed array. 

//Fix23({1, 2, 3}) ->{1, 2, 0}
//Fix23({2, 3, 5}) -> {2, 0, 5}
//Fix23({1, 2, 1}) -> {1, 2, 1}

//public int[] Fix23(int[] numbers) {

//}
    }
}
