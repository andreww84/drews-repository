﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SameFirstLast
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new int[5];
            arr1[0] = 1;
            arr1[1] = 2;
            arr1[2] = 3;
            arr1[3] = 4;
            arr1[4] = 5;

            var trueOrFalse = SameFirstLast(arr1);
            Console.WriteLine(trueOrFalse);
            Console.ReadLine();

        }

        public static bool SameFirstLast(int[] numbers)
        {
            if ((numbers.Length >= 1) && (numbers[0] == numbers[numbers.Length - 1]))
            {
                return true;
            }

            return false;
        }
    }
}
