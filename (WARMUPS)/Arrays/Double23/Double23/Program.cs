﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Double23
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = {1, 2, 3, 3};

            var result = Double23(arr1);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        public static bool Double23(int[] numbers)
        {
            var numberCounter2 = 0;
            var numberCounter3 = 0;

            foreach (var element in numbers)
            {
                if (element == 2)
                {
                    numberCounter2++;
                }
                else if (element == 3)
                {
                    numberCounter3++;
                }
                if (numberCounter2 == 2 || numberCounter3 == 2)
                {
                    return true;
                }
            }
            return false;
        }

//Given an int array, return true if the array contains 2 twice, or 3 twice. 

//Double23({2, 2, 3}) -> true
//Double23({3, 4, 5, 3}) -> true
//Double23({2, 3, 2, 2}) -> false

//public bool Double23(int[] numbers) {

//}

    }
}
