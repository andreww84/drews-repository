﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HigherWins
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new int[3] {10, 20, 30};

            var result = HigherWins(arr1);

            foreach (var number in result)
            {
                Console.WriteLine(number);
            }
            Console.ReadLine();
        }

        public static int[] HigherWins(int[] numbers)
        {
            int[] newArr = new int[3];
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[0] < numbers[2])
                {
                    newArr[0] = numbers[2];
                    newArr[1] = numbers[2];
                    newArr[2] = numbers[2];
                }
                else if (numbers[0] > numbers[2])
                {
                    newArr[0] = numbers[0];
                    newArr[1] = numbers[0];
                    newArr[2] = numbers[0];
                }
            }
            return newArr;
        }

//Given an array of ints, figure out which is larger between the first and last elements in the array, and set all the other elements to be that value. Return the changed array. 

//HigherWins({1, 2, 3}) -> {3, 3, 3}
//HigherWins({11, 5, 9}) -> {11, 11, 11}
//HigherWins({2, 11, 3}) -> {3, 3, 3}

//public int[] HigherWins(int[] numbers) {

//}
    }
}
