﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RotateRight2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word");
            var theWord = Console.ReadLine();
            var rotatedWord = RotateRight(theWord);
            Console.WriteLine(rotatedWord);
            Console.ReadLine();
        }

        public static string RotateRight(string theWord)
        {
            var rotated = theWord.Substring(0, theWord.Length - 2);
            var rotated2 = theWord.Substring(theWord.Length - 2, 2);
            return rotated2 + rotated;
        }
    }
}
