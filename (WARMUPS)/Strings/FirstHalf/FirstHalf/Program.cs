﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstHalf
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter an even numbered word");
            var theWord = Console.ReadLine();
            var halfWord = FirstHalf(theWord);
            Console.WriteLine("The first half of your word is " + halfWord);
            Console.ReadLine();
        }

        public static string FirstHalf(string theWord)
        {
            var half = (theWord.Length / 2);
            var newWord = theWord.Substring(0, half);
            return newWord;
        }
    }
}
