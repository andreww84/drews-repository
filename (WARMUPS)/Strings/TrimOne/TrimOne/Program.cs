﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrimOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word");
            var theWord = Console.ReadLine();
            var trimWord = TrimOne(theWord);
            Console.WriteLine("This is your trimmed word: " + trimWord);
            Console.ReadLine();
        }

        public static string TrimOne(string theWord)
        {
            var trim = theWord.Substring(1, theWord.Length - 2);
            return trim;
        }
    }
}
