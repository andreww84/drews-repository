﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string");
            var str1 = Console.ReadLine();
            var firstTwo = AtFirst(str1);
            Console.WriteLine(firstTwo);
            Console.ReadLine();
        }

        public static string AtFirst(string str)
        {
            string newString = null;
            if (str.Length < 2)
            {
                newString += str + "@";
            }
            else
            {
                newString += str.Substring(0, 2);
            }
            return newString;
        }

//Given a string, return a string length 2 made of its first 2 chars. If the string length is less than 2, use '@' for the missing chars. 

//AtFirst("hello") -> "he"
//AtFirst("hi") -> "hi"
//AtFirst("h") -> "h@"

//public string AtFirst(string str) {

}
    }
}
