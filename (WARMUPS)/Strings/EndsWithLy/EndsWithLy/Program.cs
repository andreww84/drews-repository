﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EndsWithLy
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter any word and I will tell you wether it ends with 'ly' or not.");
            var word = Console.ReadLine();
            var result = EndsWithLy(word);
            if (result == true)
            {
                Console.WriteLine("That word ends with 'ly'!");
            }
            else
            {
                Console.WriteLine("That word does not end with 'ly'!");
            }
            Console.ReadLine();
        }

        public static bool EndsWithLy(string word)
        {
            if (word.Substring(word.Length - 2, 2) == "ly")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }

}

