﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeTags
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What tag would you like to make?");
            var tag = Console.ReadLine();
            Console.WriteLine("What content would you like to use?");
            var content = Console.ReadLine();
            var output = MakeTags(tag, content);
            Console.WriteLine(output);
            Console.ReadLine();

        }
        public string MakeTags(string tag, string content)
        {
            var pattern = "<" + tag + ">" + content + "</" + tag + ">";
            return pattern;
        }
    }
}
