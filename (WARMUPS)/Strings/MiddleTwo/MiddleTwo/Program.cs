﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter an even letter word");
            var word = Console.ReadLine();
            var newWord = MiddleTwo(word);
            Console.WriteLine(newWord);
            Console.ReadLine();
        }

        public static string MiddleTwo(string word)
        {
            var half = (word.Length / 2);           
            var firstHalf = word.Substring(half - 1, 1);
            var secondHalf = word.Substring(half, 1);
            return firstHalf + secondHalf;
        }

    }
}
