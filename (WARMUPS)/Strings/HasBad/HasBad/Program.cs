﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HasBad
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please give me a bad string.");
            var input = Console.ReadLine();
            var callmeth = HasBad(input);
            Console.WriteLine(callmeth);
            Console.ReadLine();
        }

        public static bool HasBad(string str)
        {
            var someNumber = str.IndexOf("bad", 0);
            if (someNumber == 0 || someNumber == 1)
            {
                return true;
            }
            return false;

        }
    }


}
