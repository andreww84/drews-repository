﻿using System;

namespace TakeTwoFromPosition
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Enter a word");
            var word = Console.ReadLine();
            Console.WriteLine("Now enter a corresponding index position of your choice");
            var number = Console.ReadLine();
            var num = int.Parse(number);
            var theMethod = TakeTwoFromPosition(word, num);
            Console.WriteLine(theMethod);
            Console.ReadLine();
        }

        public static string TakeTwoFromPosition(string word, int num)
        {
            if (word.Length <= num + 1)
            {
                return word.Substring(0, 2);
            }

            return word.Substring(num, 2);
        }
    }
}