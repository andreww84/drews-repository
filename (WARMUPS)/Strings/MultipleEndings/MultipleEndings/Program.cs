﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipleEndings
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What word would you like multiple endings of?");
            var theWord = Console.ReadLine();
            var output = MultipleEndings(theWord);
            Console.WriteLine(output);
            Console.ReadLine();

        }

        public static string MultipleEndings(string theWord)
        {
            var returnStr = theWord.Substring(theWord.Length - 2, 2) + (theWord.Substring(theWord.Length - 2, 2)) + (theWord.Substring(theWord.Length - 2, 2));
            return returnStr;
        }
    }
}
