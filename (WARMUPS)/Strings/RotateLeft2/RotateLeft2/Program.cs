﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RotateLeft2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word");
            var theWord = Console.ReadLine();
            var rotatedWord = RotateLeft(theWord);
            Console.WriteLine(rotatedWord);
            Console.ReadLine();
        }

        public static string RotateLeft(string theWord)
        {
            var rotated = theWord.Substring(2, theWord.Length - 2);
            var rotated2 = theWord.Substring(0, 2);
            return rotated + rotated2;
        }
    }
}
