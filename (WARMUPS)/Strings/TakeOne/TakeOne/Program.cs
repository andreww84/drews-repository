﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeOne
{
    class Program
    {
        static void Main(string[] args)
        {
           Console.Write("Enter a word: ");
           var word = Console.ReadLine();
           Console.Clear();

           Console.Write("Would you like to print the first character? yes or no: ");
           var character = Console.ReadLine();
           bool fromFront = character == "yes";
           Console.Clear();

           var newString = TakeFrom(word, fromFront);
           Console.WriteLine(newString);
           Console.ReadLine();

       }

       public static string TakeFrom(string word, bool fromFront)
       {
           if (fromFront == true)
           {
               return word.Substring(0, 1);
           }
           else
           {
               return word.Substring(word.Length - 1, 1);           
           
       }
   }
}
        }
    

