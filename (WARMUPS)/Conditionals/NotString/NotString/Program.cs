﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotString
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string");
            var input = Console.ReadLine();
            var addStr = NotString(input);
            Console.WriteLine(addStr);
            Console.ReadLine();
        }

        public static string NotString(string s)
        {
            var newStr = "not ";
            if (s.Contains("not"))
            {
                return s;
            }
            return newStr + s;

        }
    

//Given a string, return a new string where "not " has been added to the front. However, if the string already begins with "not", return the string unchanged.

//Hint: Look up how to use "SubString" in c#

//NotString("candy") -> "not candy"
//NotString("x") -> "not x"
//NotString("not bad") -> "not bad"

//public string NotString(string s) {

//}
    }
}
