﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ParrptTrouble
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(ParrotTrouble(true, 1));
            Console.WriteLine(ParrotTrouble(true, 8));
            Console.WriteLine(ParrotTrouble(false, 5));
            Console.ReadLine();
        }

        public static bool ParrotTrouble(bool isTalking, int hour)
        {
            if (isTalking == true && ((hour < 7) || (hour > 20)))
            {
                return true;
            }
            return false;

        }
        //        We have a loud talking parrot. The "hour" parameter is the current hour time in the range 0..23. We are in trouble if the parrot is talking and the hour is before 7 or after 20. Return true if we are in trouble. 

        //ParrotTrouble(true, 6) -> true
        //ParrotTrouble(true, 7) -> false
        //ParrotTrouble(false, 6) -> false
    }
}
