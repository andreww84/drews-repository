﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace MissingChar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a string");
            var str = Console.ReadLine();
            Console.WriteLine("Now enter a string index");
            var index = int.Parse(Console.ReadLine());
            var result = MissingChar(str, index);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        public static string MissingChar(string str, int n)
        {
            if (n <= str.Length)
            {
                return str.Remove(n, 1);
            }
            return "That is not a valid index";
        }
       

//Given a non-empty string and an int n, return a new string where the char at index n has been removed. The value of n will be a valid index of a char in the original string (Don't check for bad index). 

//MissingChar("kitten", 1) -> "ktten"
//MissingChar("kitten", 0) -> "itten"
//MissingChar("kitten", 4) -> "kittn"

//public string MissingChar(string str, int n) {
  
//}
    }
}
