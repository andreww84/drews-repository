﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontAndBack
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter a word.");
            var word = Console.ReadLine();
            Console.WriteLine("Now enter an integer 1 through 3.");
            var n = Console.ReadLine();
            int num = int.Parse(n);
            var theMethod = FrontAndBack(word, num);
            Console.WriteLine(theMethod);
            Console.ReadLine();
        }

        public static string FrontAndBack(string word, int num) 
        {
            var front = word.Substring(0, num);
            var back = word.Substring(word.Length - num, num);
            return front + back;
        }
        
    }
}
