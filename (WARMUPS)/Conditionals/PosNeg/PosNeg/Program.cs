﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PosNeg
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = -1;
            int num2 = 1;
            var cond = true;
            var theMethod = PosNeg(num1, num2, cond);
            
            Console.WriteLine(theMethod);
            Console.ReadLine();
        }

        public static bool PosNeg(int a, int b, bool negative)
        {
            bool trueOrFlase =   
            if ((a < 0) && (b < 0) && true)
            {
                return true;
            }
            
            else if ((a < 0 && b > 0) || (a > 0 && b < 0))
            {
                return true;
            }
            return false;
        }


//Given two int values, return true if one is negative and one is positive. Except if the parameter "negative" is true, then return true only if both are negative. 

//PosNeg(1, -1, false) -> true
//PosNeg(-1, 1, false) -> true
//PosNeg(-4, -5, true) -> true

//public bool PosNeg(int a, int b, bool negative) {

//}
    }
}
