﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Makes10
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var alpha = 10;
            var bravo = 10;
            var make = Makes10(alpha, bravo);
            Console.WriteLine(make);
            Console.ReadLine();
        }

        public static bool Makes10(int a, int b)
        {
            if ((a == 10) || (b == 10) || (a == 10 && b == 10))
            {
                return true;
            }
            return false;
        }

//        Given two ints, a and b, return true if one if them is 10 or if their sum is 10. 

//Makes10(9, 10) -> true
//Makes10(9, 9) -> false
//Makes10(1, 9) -> true
    }
}
