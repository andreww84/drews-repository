﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MischeviousChildren
{
    class Program
    {
        static void Main(string[] args)
        {
            var childA = true;
            var childB = true;
            var decision = AreWeInTrouble(childA, childB);
            Console.WriteLine(decision);
            Console.ReadLine();
        }

        public static bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            if ((aSmile = true) && (bSmile = true))
            {
                return true;
            }

            else if ((aSmile = false) && (bSmile = false))
            {
                return true;
            }

            else
            {
                {
                    return false;
                }
            }
//We have two children, a and b, and the parameters aSmile and bSmile indicate if each is smiling. We are in trouble if they are both smiling or if neither of them is smiling. Return true if we are in trouble. 

//AreWeInTrouble(true, true) -> true
//AreWeInTrouble(false, false) -> true
//AreWeInTrouble(true, false) -> false

//public bool AreWeInTrouble(bool aSmile, bool bSmile) {
  

        }
    }
}
