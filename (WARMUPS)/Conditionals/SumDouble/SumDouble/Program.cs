﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using System.Threading.Tasks;

namespace SumDouble
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter any number");
            var input1 = Console.ReadLine();
            var num1 = int.Parse(input1);
            Console.WriteLine("Now enter a second number");
            var input2 = Console.ReadLine();
            var num2 = int.Parse(input2);
            var sum = SumDouble(num1, num2);
            Console.WriteLine(sum);
            Console.ReadLine();

        }

        public static int SumDouble(int a, int b)
        {
            if ((a < b) || (a < b))
            {
                return a + b;
            }

            else if (a == b)
            {
                return ((a + b) * 2);
            }
            return SumDouble(a, b);

        }

//Given two int values, return their sum. However, if the two values are the same, then return double their sum. 

//SumDouble(1, 2) -> 3
//SumDouble(3, 2) -> 5
//SumDouble(2, 2) -> 8
    }
}
