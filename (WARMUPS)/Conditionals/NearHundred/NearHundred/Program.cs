﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NearHundred
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = 99;
            var near = NearHundred(number);
            Console.WriteLine(near);
            Console.ReadLine();
        }

        public static bool NearHundred(int n)
        {
            if ((Math.Abs(n) < 100 && Math.Abs(n) > 89) || (Math.Abs(n) > 100 && Math.Abs(n) < 111))
            {
                return true;
            }

            else if ((Math.Abs(n) < 200 && Math.Abs(n) > 189) || (Math.Abs(n) > 200 && Math.Abs(n) < 211))
            {
                return true;
            }
            return false;
        }

//        Given an int n, return true if it is within 10 of 100 or 200.
//Hint: Check out the C# Math class for absolute value

//NearHundred(103) -> true
//NearHundred(90) -> true
//NearHundred(89) -> false

//public bool NearHundred(int n) {

//}
    }
}
