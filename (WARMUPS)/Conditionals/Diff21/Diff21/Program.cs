﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diff21
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a numer");
            var input = Console.ReadLine();
            var num = int.Parse(input);
            var absval = Diff21(num);
            Console.WriteLine(absval);
            Console.ReadLine();
        }

        public static int Diff21(int n)
        {
            if ((n - 21) > 21)
            {
                return Math.Abs(n -21) * 2;
            }

            else if ((n - 21) < 21)
            {
                return Math.Abs(n);
            }
            return Diff21(n);
        }
    }
}
