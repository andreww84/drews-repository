﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AreInOrder
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            bool result = AreInOrder.BOk(1, 2, 4, false);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void TestMethod2()
        {
            bool result = AreInOrder.BOk(1, 2, 1, false);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void TestMethod3()
        {
            bool result = AreInOrder.BOk(1, 1, 2, true);
            Assert.AreEqual(true, result);
        }
    }
}
