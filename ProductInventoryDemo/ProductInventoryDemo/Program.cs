﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;


namespace ProductInventoryDemo
{
    static public class Program
    {
        static void Main(string[] args)
        {
            Inventory inv = new Inventory();
            inv.Products.Add(new Product());
            string input = "";

            do
            {
                Console.WriteLine("What kind of product do you want to add?");
                Console.WriteLine("Press C for coat, J for blue jean, S for shirt.");
                Console.WriteLine("Press q to stop adding products");
                input = Console.ReadLine();
                Product prod = GetProductDetails(input);
                if (prod != null)
                {
                    inv.Products.Add(prod);
                }
            } while (input != "q");

            // this gets the number of shirts in my inventory....
            int quantityOfShirts = inv.Products.Where(x => x is Shirt).Count();
            
        }

        static Product GetProductDetails(string productType)
        {
            Product retProd = new Product();

            if (productType == "C")
            {
                retProd = new Coat();
                retProd = FillInProductDetails(retProd);

                Console.WriteLine("Enter in whether or not the coat is fur.  Enter True for yes or False for no");
                var torfStr = Console.ReadLine();
                ((Coat)retProd).IsFur = bool.Parse(torfStr);

                Console.WriteLine("Enter in the length");
                var lenStr = Console.ReadLine();
                ((Coat)retProd).Length = int.Parse(lenStr);

                return retProd;
            }
            else if (productType == "J")
            {
                // add jeans code here
                retProd = new BlueJean();
                retProd = FillInProductDetails(retProd);
                
                Console.WriteLine("Enter the waiste size");
                var waist = Console.ReadLine();
                ((BlueJean) retProd).WaistSize = int.Parse(waist);

                return retProd;
            }
            else if (productType == "S")
            {
                //add shirt code here;
                retProd = new Shirt();
                retProd = FillInProductDetails(retProd);

                Console.WriteLine("Enter the neck size");
                var neck = Console.ReadLine();
                ((Shirt) retProd).NeckSize = int.Parse(neck);

                Console.WriteLine("Enter the sleeve length");
                var sleeve = Console.ReadLine();
                ((Shirt) retProd).SleeveLength = int.Parse(sleeve);

                return retProd;
            }
            else
            {
                return null;
            }
        }

        static Product FillInProductDetails(Product prod)
        {
            // fill in the rest of the product properties
            Console.WriteLine("Please enter the Name of the product");
            string prodname = Console.ReadLine();
            Console.WriteLine("Please enter the product Price");
            string decimalValue = Console.ReadLine();
            prod.Price = Decimal.Parse(decimalValue);
            Console.WriteLine("Please enter a product Description");
            string describe = Console.ReadLine();
            Console.WriteLine("Please enter a product Color");
            var color = Console.ReadLine();
            Console.WriteLine("Please enter the product SKU");
            var barcode = int.Parse(Console.ReadLine());
            
            return prod;
        }
    }
}
