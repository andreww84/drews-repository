﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProductInventoryDemo
{
    public class Barcode : IComparable
    {
        public string Upc { get; set; }
        
        // my recommendation is to convert the underlying
        // UPC to a long (since it's really just a big number)
        // and return the result of comparing one long to another
        public int CompareTo(object obj)
        {
            var obj1 = obj.ToString();
            var toLong = int.Parse(obj1);
            return toLong.CompareTo(obj);
        }
    }
}
