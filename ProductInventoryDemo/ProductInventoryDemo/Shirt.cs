﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductInventoryDemo
{


    public class Shirt : Product
    {
        public int NeckSize { get; set; }
        public int SleeveLength { get; set; }
    }
}
