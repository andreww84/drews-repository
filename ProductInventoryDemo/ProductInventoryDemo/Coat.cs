﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductInventoryDemo
{
    public class Coat : Product
    {
        public bool IsFur { get; set; }
        public int Length { get; set; }
    }
}
