﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Lab_Employee
{
    class Departments
    {
        public enum DeptList
        {
            Accounting,
            Customer_Service,
            Human_Resources,
            IT,
            Sales
        };

        public enum DeptLocations
        {
            First_Floor,
            Second_Floor,
            Third_Floor
        };
    }
}
