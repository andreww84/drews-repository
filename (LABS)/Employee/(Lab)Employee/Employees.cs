﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Lab_Employee
{
    class Employees
    {
        public string _firstName { get; set; }
        public string _lastName { get; set; }
        public string _title { get; set; }
        public string _hireDate { get; set; }
        public string _dept { get; set; }
        public string _dept2 { get; set; }
        public string _deptLocation { get; set; }
        public string _deptLocation2 { get; set; }

        public string GetName()
        {
            return string.Format("Name: {0} {1}", _firstName, _lastName);
        }

        public string GetTitle()
        {
            return string.Format("Title: {0}", _title);
        }

        public string GetHireDate()
        {
            return string.Format("Hire Date: {0}", _hireDate);
        }

        public string GetDept()
        {
            return string.Format("Dept: {0}", _dept);
        }

        public string GetDeptLoc()
        {
            return string.Format("Dept Location: {0}", _deptLocation);
        }

        public string SayHello(Employees name)
        {
            return string.Format("Hello, my name is {0}", name._firstName);
        }

        public string Greeting(Employees otherName)
        {
            return string.Format("Hello {0}, my name is {1}.  Nice to meet you!", otherName._firstName, this._firstName);
        }
    }
}
