﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace _Lab_Employee
{
    class Program
    {
        static void Main(string[] args)
        {
            Employees emp1 = new Employees()
            {
                _firstName = "Andrew",
                _lastName = "Ward",
                _title = ".NET Developer",
                _dept = Departments.DeptList.IT.ToString().Replace("_", " "),
                _deptLocation = Departments.DeptLocations.First_Floor.ToString().Replace("_", " "),
                _hireDate = "5/11/2015"
            };

            Employees emp2 = new Employees()
            {
                _firstName = "Mandi",
                _lastName = "Schoener",
                _title = "HR Specialist Sr.",
                _dept = Departments.DeptList.Human_Resources.ToString().Replace("_", " "),
                _dept2 = Departments.DeptList.Customer_Service.ToString().Replace("_", " "),
                _deptLocation = Departments.DeptLocations.Third_Floor.ToString().Replace("_", " "),
                _deptLocation2 = Departments.DeptLocations.Second_Floor.ToString().Replace("_", " "),
                _hireDate = "5/20/2015"

            };

            Console.WriteLine(emp1.GetName());
            Console.WriteLine(emp1.GetTitle());
            Console.WriteLine(emp1.GetHireDate());
            Console.WriteLine(emp1.GetDept());
            Console.WriteLine(emp1.GetDeptLoc());
            Console.ReadLine();

            Console.WriteLine(emp1.SayHello(emp1));
            Console.ReadLine();

            Console.WriteLine(emp2.Greeting(emp1));
            Console.ReadLine();

            Console.WriteLine(emp2.GetName());
            Console.WriteLine(emp2.GetTitle());
            Console.WriteLine(emp2.GetHireDate());
            Console.WriteLine(emp2.GetDept());
            Console.WriteLine(emp2.GetDeptLoc());
            Console.ReadLine();
        }

    }
}
