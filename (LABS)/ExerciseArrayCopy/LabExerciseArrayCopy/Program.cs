﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace LabExerciseArrayCopy
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = {1, 3, 5, 7, 9};
            var result = CopyArray(arr1);

            foreach (var element in result)
            {
                Console.WriteLine(element);
            }
            Console.ReadLine();
        }

        public static int[] CopyArray(int[] n)
        {
            int[] newArr = new int[n.Length];

            for (int i = 0; i < n.Length; i++)
            {
                newArr = n;
            }
            return newArr;
        }
        
    }
}
