﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Lab_Car
{
    class Car
    {
        public const string year = "2000";
        public const string make = "Mazda";
        public const string model = "Miata";
        public string color { get; set; }
        public const string doors = "2 Door";

        public string PrintInfo()
        {
            return year + " " + make + " " + model + " " + color + " " + doors;
        }
    }
}
