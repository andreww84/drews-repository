﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Lab_Car
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car();

            car1.color = "Black";

            Console.WriteLine(car1.PrintInfo());
            Console.ReadLine();

            car1.color = ("White");
            Console.WriteLine(car1.PrintInfo());
            Console.ReadLine();
        }
    }

//Car Project
//Difficulty: 1

//Create a console program and a class that models a car. In our case, we want to track the state of the color, make, model, year, and number of doors the car currently has. In addition we want to be able to paint the car a different color.

//Intially the car should be a 2000 black Mazda Miata.

//At the end of our program it should be a 2000 white Mazda Miata.

//Tips
//----------------------------
//Remember that generally speaking, adjectives and nouns are properties and that actions and verbs are methods.

//Added Difficulty
//----------------------------
//Make sure that only the color can be changed after the car has been created.
        
    
}
