﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Web;
using Antlr.Runtime.Tree;

namespace CarDealership.Models
{
    public class ContactForm : IValidatableObject
    {
        
        public string Name { get; set; }
        public int PurchaseTimeFrameInMonths { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public decimal? Income { get; set; }
        public int PayFrequency { get; set; }



        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (Name == null)
            {
                errors.Add(new ValidationResult("Please enter your name.", new[] { "Name" }));
            }

            if ((PhoneNumber == null) || (!(PhoneNumber.Length == 14 && PhoneNumber.Substring(0, 1) == "1" && PhoneNumber.Substring(1, 1) == "-" &&
               PhoneNumber.Substring(5, 1) == "-" && PhoneNumber.Substring(9, 1) == "-")))
            {
                errors.Add(new ValidationResult("Please enter a valid phone number ex. 1-234-567-8910.", new[] { "PhoneNumber" }));
            }

            if (PayFrequency == (0))
            {
                errors.Add(new ValidationResult("Please choose pay frequency.", new [] { "PayFrequency" }));
            }

            if (PayFrequency == (1))
            {
                Income = Income;
            }

            if (PayFrequency == (2))
            {
                Income = Income * 12;
            }

            if (PayFrequency == (3))
            {
                Income = Income * 26;
            }

            if (PayFrequency == (4))
            {
                Income = Income * 52;
            }

            if (Income < 10000M && PurchaseTimeFrameInMonths > (12))
            {
                errors.Add(new ValidationResult("Sorry, you're not a serious buyer and don't make enough money, thus we don't want your business."));
            }

            if (Income == null)
            {
                errors.Add(new ValidationResult("Please enter your income.", new [] { "Income" }));
            }

            if (PurchaseTimeFrameInMonths == (0))
            {
                errors.Add(new ValidationResult("Please choose an estimated time frame.", new [] { "PurchaseTimeFrameInMonths" }));
            }

            if (Email == null || !Email.Contains("@"))
            {
                errors.Add(new ValidationResult("Please enter a valid email address.", new[] {"Email"}));
            }
            
            return errors;
        }
    }
}