﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Lab_GradeBook
{
    public class Assignment
    {
        public string Name { get; set; }

        private int _Score;

        public int Score
        {
            get
            {
                return _Score;
            }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    _Score = value;
                }
                else
                {
                    throw new ArgumentException("Score must be between 0 and 100.");
                }
            }
        }

    }
}

