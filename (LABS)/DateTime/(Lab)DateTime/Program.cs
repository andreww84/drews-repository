﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace _Lab_DateTime
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime inputtedDate;
            string input;
            do
            {
                Console.WriteLine("Please enter a date:");
                input = Console.ReadLine();
            } while (!DateTime.TryParse(input, out inputtedDate));

            int numWednesdays = GetWednesdays(DateTime.Today, inputtedDate);

            Console.WriteLine("There are {0} Wednesdays between today and {1}", numWednesdays, inputtedDate.ToString("d"));
            Console.ReadLine();
        }

        public static int GetWednesdays(DateTime startDate, DateTime endDate)
        {
                int numWednesdays = 0;

            do
            {
                if (startDate.AddDays(7) <= endDate)
                {
                    numWednesdays++;
                }
                startDate = startDate.AddDays(7);

            } while (startDate <= endDate);


            return numWednesdays;
        }
    }
}
