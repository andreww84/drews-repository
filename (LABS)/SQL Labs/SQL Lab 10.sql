SELECT TOP(2) LastName, FirstName, MAX(HireDate) AS HireDate
FROM Employee
GROUP BY LastName, FirstName
ORDER BY HireDate


SELECT TOP(6) WITH TIES *
FROM [Grant]
ORDER BY Amount


SELECT TOP(10) *
FROM CurrentProducts
WHERE Category = 'No-Stay'
ORDER BY RetailPrice DESC
