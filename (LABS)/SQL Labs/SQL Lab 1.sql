SELECT * 
FROM Products
WHERE ProductName = 'Queso Cabrales'

SELECT ProductName, UnitsInStock
FROM Products
WHERE ProductName IN ('Laughing Lumberjack Lager', 'Outback Lager', 'Ravioli Angelo')

SELECT *
FROM Customers
WHERE CustomerID = 'QUEDE'

SELECT *
FROM Orders
WHERE Freight > 100.00

SELECT *
FROM Orders
WHERE ShipCountry = 'USA' AND Freight BETWEEN 10.00 AND 20.00

SELECT LastName, FirstName, EmployeeTerritories.TerritoryID, Territories.TerritoryDescription
FROM Employees
	INNER JOIN EmployeeTerritories
	ON Employees.EmployeeID = EmployeeTerritories.EmployeeID
	INNER JOIN Territories
	ON EmployeeTerritories.TerritoryID = Territories.TerritoryID

SELECT ContactName, OrderDate, ProductName
FROM Customers
	INNER JOIN Orders
	ON Customers.CustomerID = Orders.CustomerID
	INNER JOIN [Order Details]
	ON Orders.OrderID = [Order Details].OrderID
	INNER JOIN Products
	ON [Order Details].ProductID = Products.ProductID
WHERE ShipCountry = 'USA'

SELECT *
FROM Orders, [Order Details], Products
WHERE ProductName = 'Chai'

SELECT OrderDate
FROM Orders
WHERE ShipCountry = 'Brazil'

SELECT LastName, FirstName, EmployeeTerritories.TerritoryID, Territories.TerritoryDescription
FROM Employees
	INNER JOIN EmployeeTerritories
	ON Employees.EmployeeID = EmployeeTerritories.EmployeeID
	INNER JOIN Territories
	ON EmployeeTerritories.TerritoryID = Territories.TerritoryID
WHERE Employees.City = 'Seattle'

SELECT DISTINCT RegionID
FROM Territories

SELECT OrderID, SUM ([Order Details].UnitPrice * [Order Details].Quantity) AS GrandTotal
FROM [Order Details]
GROUP BY OrderID

SELECT Customers.CustomerID, CompanyName, SUM ([Order Details].UnitPrice * [Order Details].Quantity) AS GrandTotal
FROM Customers
	LEFT JOIN Orders
	ON Customers.CustomerID = Orders.CustomerID
	LEFT JOIN [Order Details]
	ON Orders.OrderID = [Order Details].OrderID
GROUP BY CompanyName, Customers.CustomerID

SELECT SUM ([Order Details].UnitPrice * [Order Details].Quantity) AS GrandTotal
FROM [Order Details]
	
	


