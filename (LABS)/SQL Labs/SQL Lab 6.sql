DECLARE @MinimumValue smallmoney
DECLARE @MaximumValue smallmoney
SET @MinimumValue = 15000
SET @MaximumValue = 20000

SELECT *
FROM [Grant]
WHERE Amount BETWEEN @MinimumValue AND @MaximumValue



CREATE PROCEDURE GetProductListByCategory
(
	@CategoryName varchar(20)
) AS

SELECT *
FROM CurrentProducts
WHERE Category = @CategoryName
GO
EXEC GetProductListByCategory 'LongTerm-Stay'



CREATE PROCEDURE GetGrantsByEmployee
(
	@LastName varchar(20)
) AS

SELECT Employee.EmpID, FirstName, LastName, GrantName, Amount
FROM Employee
	INNER JOIN [Grant]
	ON Employee.EmpID = [Grant].EmpID
WHERE LastName = @LastName
GO
EXEC GetGrantsByEmployee 'Osako'



CREATE PROCEDURE UpdateGrantTable
(
	@GrantID char(3),
	@GrantName nvarchar(50),
	@EmpID int,
	@Amount smallmoney
) AS

UPDATE [Grant] SET
	GrantID = @GrantID,
	GrantName = @GrantName,
	EmpID = @EmpID,
	Amount = @Amount
WHERE
	GrantID = @GrantID
GO

EXEC UpdateGrantTable '005', 'BIG 6''s Foundation', 4, 21000.00



CREATE PROCEDURE InsertGrantTable
(
	@GrantID char(3),
	@GrantName nvarchar(50),
	@EmpID int,
	@Amount smallmoney
) AS

INSERT INTO [Grant] (GrantID, GrantName, EmpID, AMount)
VALUES ( @GrantID, @GrantName, @EmpID, @Amount)
GO

EXEC InsertGrantTable '011', '1800CARTPETSDOTCOM', 11, 100000.00

SELECT *
FROM [Grant]



SELECT *
FROM [Grant]
ORDER BY GrantName



SELECT EmpID, LastName, FirstName
FROM Employee
ORDER BY HireDate DESC



SELECT ProductName, Category
FROM CurrentProducts
ORDER BY RetailPrice DESC



SELECT GrantName, Amount
FROM [Grant]
ORDER BY Amount DESC, GrantName



SELECT FirstName, LastName, City
FROM Employee
FULL OUTER JOIN Location
ON Employee.LocationID = Location.LocationID
ORDER BY City





