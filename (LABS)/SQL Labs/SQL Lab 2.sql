CREATE TABLE [dbo].[Customers](
[CustomerID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
[CustomerType] [varchar](30) NOT NULL,
[FirstName] [varchar](20) NULL,
[LastName] [varchar](30) NULL,
[CompanyName] [varchar](30) NULL)

INSERT INTO Customers (CustomerType, FirstName, LastName, CompanyName)
VALUES ('Consumer', 'Mark', 'Williams', NULL),
	('Consumer', 'Lee', 'Young', NULL),
	('Consumer', 'Patricia', 'Martin', NULL),
	('Consumer', 'Mary', 'Lopez', NULL),
	('Business', NULL, NULL, 'MoreTechnology.com')

SELECT * FROM Customers