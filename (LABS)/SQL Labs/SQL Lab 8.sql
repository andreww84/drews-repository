SELECT EmpID, MAX(Amount) AS MaximumAmount, MIN(Amount) AS MinimumAmount, AVG(amount) AS AverageAmount
FROM [Grant]
WHERE EmpID IS NOT NULL
GROUP BY EmpID


SELECT MAX(Amount) AS MaximumAmount, MIN(Amount) AS MinimumAmount, AVG(amount) AS AverageAmount
FROM [Grant]
GROUP BY EmpID


--THIS WILL ERROR
SELECT Employee.EmpID, Employee.FirstName, Employee.LastName, MAX(Amount) AS MaxGrant
FROM [Grant]
	INNER JOIN Employee ON [Grant].EmpID = [Grant].EmpID
	--ERROR BECAUSE NOT ALL SELECT FIELDS ARE BEING USED IN GROUP BY
GROUP BY Employee.EmpID  
ORDER BY MaxGrant DESC

--THIS WILL NOT ERROR
SELECT Employee.EmpID, Employee.FirstName, Employee.LastName, MAX(Amount) AS MaxGrant
FROM [Grant]
	INNER JOIN Employee ON [Grant].EmpID = [Grant].EmpID
	--BECAUSE AGGREGATE, GROUP BY MUST CONTAIN ALL SELECT FIELDS
GROUP BY Employee.EmpID, Employee.FirstName, Employee.LastName  
ORDER BY MaxGrant DESC




