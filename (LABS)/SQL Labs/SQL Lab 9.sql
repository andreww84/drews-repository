SELECT ProductName, (UnitPrice * UnitsInStock) AS StockValue
FROM Products
ORDER BY StockValue DESC


SELECT (LastName + ', ' + FirstName) AS NameLastFirst
FROM Employees
ORDER BY LastName, FirstName


SELECT ProductName, (UnitPrice * UnitsInStock) AS StockValue, 
		(UnitPrice * UnitsInStock) * 1.23 AS CanadianDollars, (UnitPrice * UnitsInStock) * 123.57 AS Yen, 
		(UnitPrice * UnitsInStock) *.89 AS Euros, (UnitPrice * UnitsInStock) * 15.47 AS Pesos
FROM Products
ORDER BY StockValue DESC


SELECT CategoryID, MAX(UnitPrice) AS MaxUnitPrice
FROM Products
GROUP BY CategoryID
ORDER BY CategoryID


SELECT CustomerID, COUNT(Orders.OrderID) AS MostOrdersSubmitted, SUM([Order Details].UnitPrice * [Order Details].Quantity) AS LifeTimeOrderTotal
FROM Orders
	INNER JOIN [Order Details] ON Orders.OrderID = [Order Details].OrderID
GROUP BY CustomerID
ORDER BY LifeTimeOrderTotal DESC


SELECT CompanyName, COUNT(Orders.OrderID) AS OrderCount
FROM Customers
	INNER JOIN Orders ON Customers.CustomerID = Orders.CustomerID
GROUP BY CompanyName
HAVING Count(Orders.OrderID) > 10
ORDER BY Count(Orders.OrderID) DESC


SELECT (LastName + ', ' + FirstName) as EmployeeName, COUNT(Orders.OrderID) AS OrderCount
FROM Employees
	INNER JOIN Orders ON Employees.EmployeeID = Orders.EmployeeID
GROUP BY LastName, FirstName
HAVING Count(Orders.OrderID) > 100
ORDER BY Count(Orders.OrderID) DESC





