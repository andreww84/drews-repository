ALTER TABLE Movie
	ADD [Release Year] VARCHAR(100) NULL

ALTER TABLE Movie
	ADD [Teaser] VARCHAR(200) NOT NULL
	DEFAULT 'Teaser'

EXEC sp_rename 'Movie.Runtime', 'RuntimeMinutes'

ALTER TABLE Movie
	ALTER COLUMN [Release Year] VARCHAR(4)

EXEC sp_rename 'Movie.Release Year', 'ReleaseYear'