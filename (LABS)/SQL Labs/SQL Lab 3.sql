SELECT * FROM Employee
CROSS JOIN Location 
WHERE Employee.EmpID IN (1,2)


SELECT *
FROM Employee
	LEFT OUTER JOIN [Grant]
	ON Employee.EmpID = [Grant].EmpID
WHERE GrantName IS NULL


UPDATE Employee
	SET LastName = 'Green'
FROM Employee
WHERE EmpID = 11


UPDATE Employee
	SET [Status] = 'External'
FROM Employee
	INNER JOIN Location ON Employee.LocationID = Location.LocationID
WHERE City = 'Spokane'


UPDATE Location	
	SET Street = '111 1st Ave'
FROM Location
WHERE City = 'Seattle'


UPDATE [Grant]
	SET Amount = 20000.00
FROM [Grant]
	INNER JOIN Employee ON [Grant].EmpID = [Grant].EmpID
	INNER JOIN Location ON Employee.LocationID = Location.LocationID
WHERE City = 'Boston'


SELECT * /*TEST*/
FROM [Grant]
	INNER JOIN Employee ON [Grant].EmpID = [Grant].EmpID
	INNER JOIN Location ON Employee.LocationID = Location.LocationID
WHERE City = 'Boston'


DELETE FROM MgmtTraining
WHERE ClassDurationHours > 20


