﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarDealership.Models
{
    public class Car : IValidatableObject
    {
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (Make == null)
            {
                errors.Add(new ValidationResult("Please enter the vehicle Make.", new [] { "Make" }));
            }

            if (Model == null)
            {
                errors.Add(new ValidationResult("Please enter the vehicle Model.", new [] { "Model" }));
            }

            if ((Year == null) || (Year.Length != (4)))
            {
                errors.Add(new ValidationResult("Please enter the four digit vehicle Year.", new [] { "Year" }));
            }

            if (Title == null)
            {
                errors.Add(new ValidationResult("Please enter the vehicle Description.", new [] { "Title" }));
            }

            if ((Price == null) || (Price < 1000M))
            {
                errors.Add(new ValidationResult("Please enter a vehicle Price. Vehicle Price must be at least $1,000.00", new [] { "Price" }));
            }

            return errors;
        }
    }
}