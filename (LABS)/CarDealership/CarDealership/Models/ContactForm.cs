﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarDealership.Models
{
    public class ContactForm : IValidatableObject
    {
        // Your task is to:
        //
        // Make all fields required-(DONE)
        // Ensure the Email field contains an '@' symbol-(DONE)
        // Ensure PhoneNumber is in the format: 1-XXX-XXX-XXXX-(DONE)
        // If the Income is less than 10000 and the PurchaseTimeFrameInMonts is greater than 12,
        // generate a model level area that says 'We don't want your business!'-(DONE)
        // On the page that lists car images, add a 'Add new car' link that takes the user to a view that allows them to add a car to the database. This is going to require two actions.
        // On the Details page for each car, add an edit button that takes you to a page where you can edit a car. Also add a link that takes the user to a page that is a confirmation that they want to delete it and then allows them actually delete it.
        // Add server side validation: Model, Make, Year, Title and Price are required. Year must be exactly of length 4, and Price must be greater than $1,000
        // IF you get done with all that, go ahead and add routing that looks like /Car/Year/Make/Model that will take you directly to that car. So for example if someone types /Car/1996/Honda/Accord, it would take them directly to the details page for that car.
        // If you get done with all that and want more to do, email me and I'll get you more to do

        public string Name { get; set; }
        public int PurchaseTimeFrameInMonths { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public decimal? Income { get; set; }
        public int PayFrequency { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (Name == null)
            {
                errors.Add(new ValidationResult("Please enter your name.", new[] { "Name" }));
            }

            if ((PhoneNumber == null) || (!(PhoneNumber.Length == 14 && PhoneNumber.Substring(0, 1) == "1" && PhoneNumber.Substring(1, 1) == "-" &&
               PhoneNumber.Substring(5, 1) == "-" && PhoneNumber.Substring(9, 1) == "-")))
            {
                errors.Add(new ValidationResult("Please enter a valid phone number ex. 1-234-567-8910.", new[] { "PhoneNumber" }));
            }

            if (PayFrequency == (0))
            {
                errors.Add(new ValidationResult("Please choose pay frequency.", new[] { "PayFrequency" }));
            }

            if (PayFrequency == (1))
            {
                Income = Income;
            }

            if (PayFrequency == (2))
            {
                Income = Income * 12;
            }

            if (PayFrequency == (3))
            {
                Income = Income * 26;
            }

            if (PayFrequency == (4))
            {
                Income = Income * 52;
            }

            if (Income < 10000M && PurchaseTimeFrameInMonths > (12))
            {
                errors.Add(new ValidationResult("Sorry, you're not a serious buyer and don't make enough money, thus we don't want your business."));
            }

            if (Income == null)
            {
                errors.Add(new ValidationResult("Please enter your income.", new[] { "Income" }));
            }

            if (PurchaseTimeFrameInMonths == (0))
            {
                errors.Add(new ValidationResult("Please choose an estimated time frame.", new[] { "PurchaseTimeFrameInMonths" }));
            }

            if (Email == null || !Email.Contains("@"))
            {
                errors.Add(new ValidationResult("Please enter a valid email address.", new[] { "Email" }));
            }

            return errors;
        }
    }
}