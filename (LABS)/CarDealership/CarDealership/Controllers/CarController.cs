﻿using CarDealership.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using System.Web.WebPages;
using CarDealership.Models;

namespace CarDealership.Controllers
{
    public class CarController : Controller
    {
        ICarRepository _repo = new DBCarRepository();

        // GET: Car
        public ActionResult Index()
        {
            var cars = _repo.GetAllCars();
            return View(cars);
        }

        public ActionResult Details(int id)
        {
            var car = _repo.GetCarById(id);
            return View(car);
        }

        public ActionResult Add()
        {
            return View("Add");
        }

        public ActionResult Confirmation()
        {
            return View("Confimation");
        }        

        public ActionResult Edit(int id)
        {
            Car car = _repo.GetCarById(id);
            return View(car);
        }

        public ActionResult Delete(int id)
        {
            Car car = _repo.GetCarById(id);
            return View(car);
        }

        [HttpPost]
        public ActionResult AddCar(Car car)
        {
            ICarRepository _car = new DBCarRepository();
            ViewBag.Message = "Add a new vehicle by filling out the form below.";

            car.Make = Request.Form["Make"];
            car.Model = Request.Form["Model"];
            car.Year = Request.Form["Year"];
            car.ImageUrl = Request.Form["ImageUrl"];
            car.Title = Request.Form["Title"];
            car.Description = Request.Form["Description"];
            car.Price = Request.Form["Price"].AsDecimal();

            if (ModelState.IsValid)
            {
                _car.AddCar(car);
                return RedirectToAction("Confirmation");
            }
            else
            {
                return View("Add");
            }
        }

        public ActionResult EditCar(Car car)
        {
            if (ModelState.IsValid)
            {
                _repo.EditCar(car);
                return RedirectToAction("Index");
            }
            else
            {
                return View("Edit");
            }
        }

        public ActionResult DeleteCar(int id)
        {
            _repo.DeleteCar(id);
                return View("Deleted");
        }
    }
}