﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BFQuickLoadOut.Repositories
{
    public class DBWeaponRepository : IWeaponRepository
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["BFQuickLoadout"].ConnectionString;

        public List<Models.Weapon> GetAssaultRifles()
        {
            List<Models.Weapon> assaultRifles = new List<Models.Weapon>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("SELECT * FROM Weapons WHERE WeaponType = 'Assault'", conn);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Models.ToDoItem newItem = new Models.ToDoItem();
                    newItem.Id = reader.GetInt32(0);
                    newItem.ToDo = reader.GetString(1);
                    newItem.DueDate = reader.GetDateTime(2);
                    newItem.IsCompleted = reader.GetBoolean(3);
                    todoItems.Add(newItem);
                }
            }
        }
    }
}