﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BFQuickLoadOut.Models
{
    public class Shotgun : Weapon
    {
        public int Pellets { get; private set; }
        public bool IsBuckshot { get; private set; }
        public bool IsFlechette { get; private set; }
        public bool IsFrag { get; private set; }
        public bool IsSlug { get; private set; }

        public Shotgun(int pellets, bool isBuck, bool isFlech, bool isFrag, bool isSlug)
        {
            Pellets = pellets;
            IsBuckshot = isBuck;
            IsFlechette = isFlech;
            IsFrag = isFrag;
            IsSlug = isSlug;
        }
    }
}