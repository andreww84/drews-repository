﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BFQuickLoadOut.Models;

namespace BFQuickLoadOut.Controllers.api
{
    public class WeaponTypeApiController : ApiController
    {
        public List<Weapon> Get(string id)
        {
            return new List<Weapon>().Where(x=> x.WeaponType == id).ToList();
        }
    }
}
