﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BFQuickLoadOut.Startup))]
namespace BFQuickLoadOut
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
