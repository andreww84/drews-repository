﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod20
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a non-negative number");
            var userInput = int.Parse(Console.ReadLine());
            var result = Mod20(userInput);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        public static bool Mod20(int n)
        {
            if ((n > 0 && n%20 == 0) || (n > 0 && n%20 == 0 + 1) || (n > 0 && n%20 == 0 + 2))
            {
                return true;
            }
            return false;
        }

//Return true if the given non-negative number is 1 or 2 more than a multiple of 20. See also: Introduction to Mod 

//Mod20(20) → false
//Mod20(21) → true
//Mod20(22) → true

//public bool Mod20(int n) {
  
//}
    }
}
