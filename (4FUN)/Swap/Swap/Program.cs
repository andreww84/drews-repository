﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swap
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = 11;
            Swap(a, b);
        }

        public static void Swap (int a, int b)
        {
            if (a != b)
            {
                a = a + b;
                b = a - b;
                a = a - b;

                Console.WriteLine(a + " " + b);
                Console.ReadLine();
            }
        }
    }
}
