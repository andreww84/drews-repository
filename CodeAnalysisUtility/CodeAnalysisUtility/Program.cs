﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CodeAnalysisUtility
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter a directory: ");
            string userInput = Console.ReadLine();

            //If invalid directory is entered, allow user to try again.
            while(!Directory.Exists(userInput))
            {
                Console.Write("That directory does not exist. Please enter a directory: ");
                userInput = Console.ReadLine();
            }
            //validation
            if (Directory.Exists(userInput))
            {
                var linesOfCode = AnalyzeCode(userInput);
                var filesCounted = AnalyzeFiles(userInput);
                Console.WriteLine("There were " + linesOfCode + " lines of code counted from " + filesCounted + " file/s.");
                Console.ReadLine();
            }
            else Console.WriteLine("That directory or file path does not exist.");
                Console.ReadLine();
        }

        //Counts the lines of code.  *Must have permissions to access files.*  I created a seperated directory under C:\ with dummy files containing code for this to work.
        public static int AnalyzeCode (string userInput)
        {   
            var allFiles = Directory.GetFiles(userInput, "*.*", SearchOption.AllDirectories);
            //Filters the file extensions.
            var filteredFiles = allFiles.Where(f => f.EndsWith(".cs") || f.EndsWith(".aspx") ||
                 f.EndsWith(".ascx") || f.EndsWith(".txt") || f.EndsWith(".html") || f.EndsWith(".htm") || f.EndsWith(".js") ||
                 f.EndsWith(".xsl") || f.EndsWith(".xslt"));
            var lineCount = filteredFiles.Sum(file => 
                    {
                        int lines = 0;
                        using (StreamReader reader = new StreamReader(file))
                            while (reader.ReadLine() != null)
                            {
                                lines++;
                            }
                        return lines;
                    });
            return lineCount;
        }

        //Number of files code was counted for.
        public static int AnalyzeFiles(string userInput)
        {
            var files = Directory.GetFiles(userInput, "*.*", SearchOption.AllDirectories);
        //Filters the file extensions.
            var filteredFiles = files.Where(f => f.EndsWith(".cs") || f.EndsWith(".aspx") ||
                f.EndsWith(".ascx") || f.EndsWith(".txt") || f.EndsWith(".html") || f.EndsWith(".htm") || f.EndsWith(".js") ||
                f.EndsWith(".xsl") || f.EndsWith(".xslt"));

            return filteredFiles.Count();
        }        
    }       
}
