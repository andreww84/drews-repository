﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TwoDice100Times
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> dice = new List<int>();
            var rollCount = 0;
            Random rng1 = new Random();

            for (int i = 1; i <= 100; i++)
            {
                int diceRoll = rng1.Next(2, 13);
                dice.Add(diceRoll);
            }
          
            for (int i = 2; i <= 12; i++)
            {
                foreach (var elem in dice)
                {
                    if (i == elem)
                    {
                        rollCount++;
                    }
                }

                Console.WriteLine("You rolled {0}, {1} times", i, rollCount);
                rollCount = 0;
            }
            Console.ReadLine();
        }
    }
}
