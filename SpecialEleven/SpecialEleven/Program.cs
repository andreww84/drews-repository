﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecialEleven
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a non-negative number");
            var userInput = int.Parse(Console.ReadLine());
            var result = SpecialEleven(userInput);
            Console.WriteLine(result);
            Console.ReadLine();
        }

        public static bool SpecialEleven(int n)
        {
            if (n > 0 && n % 11 == 0 || n > 0 && n % 11 == 0 + 1)
            {
                return true;
            }
            return false;
        }

//We'll say a number is special if it is a multiple of 11 or if it is one more than a multiple of 11. Return true if the given non-negative number is special. Use the % "mod" operator

//SpecialEleven(22) → true
//SpecialEleven(23) → true
//SpecialEleven(24) → false

//public bool SpecialEleven(int n) {

//}
    }
}
