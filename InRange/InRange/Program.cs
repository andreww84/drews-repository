﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using System.Threading.Tasks;

namespace InRange
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press \"y\" to enter a number 1 through 10.  Press \"n\" to enter a number outside of that range");
            var userInput = Console.ReadLine();

            if (userInput == "y")
            {
                Console.WriteLine("Enter a number 1 through 10");
                var num = int.Parse(Console.ReadLine());
                var result = InRange(num, false);
                Console.WriteLine(result);
                Console.ReadLine();
            }
            else if (userInput == "n")
            {
                Console.WriteLine("Enter a number outside of the range 1 through 10");
                var outNum = int.Parse(Console.ReadLine());
                var result2 = InRange(outNum, true);
                Console.WriteLine(result2);
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Please enter a valid response");
                Console.ReadLine();
            }
}

        public static bool InRange(int n, bool outsideMode)
        {
            if ((n >= 1 && n <= 10) && outsideMode == false)
            {
                return true;
            }
            else if ((n <= 1 || n >= 10) && outsideMode == true)
            {
                return true;
            }
            return false;
        }

//Given a number n, return true if n is in the range 1..10, inclusive. Unless "outsideMode" is true, in which case return true if the number is less or equal to 1, or greater or equal to 10. 

//InRange(5, false) → true
//InRange(11, false) → false
//InRange(11, true) → true

//public bool InRange(int n, bool outsideMode) {
  
//}
    }
}
