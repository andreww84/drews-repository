﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace ArrayLab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            Console.WriteLine("Please enter a string to reverse.");
            var input = Console.ReadLine();
            var reverse = p.ReverseString(input);
            Console.WriteLine(reverse);
            Console.ReadLine();
        }

        public string ReverseString(string str)
        {
            char[] charArr = str.ToCharArray();
            char[] rCharArr = charArr.Reverse().ToArray();

            string s = "";
            foreach (char c in rCharArr)
            {
                s = s + c;
            }

            return s;
        }
    }
}
