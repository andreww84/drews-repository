﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rng = new Random();
            int numberToGuess = rng.Next(1, 21);
            string input;
            int playerGuess;
            bool isNumberGuessed = false;
            List<int> guesses = new List<int>();

            do
            {
                Console.WriteLine("Please guess a number");
                input = Console.ReadLine();

                if (int.TryParse(input, out playerGuess))
                {
                    if (guesses.Contains(playerGuess))
                    {
                        Console.WriteLine("You already guessed that number");
                    }
                    else
                    {
                        guesses.Add(playerGuess);
                    }
                    if (playerGuess == numberToGuess)
                    {
                        Console.WriteLine("You Won!!");
                        isNumberGuessed = true;
                    }
                    else
                    {
                        if (playerGuess < numberToGuess)
                        {
                            Console.WriteLine("Too low!");
                        }
                        else
                        {
                            Console.WriteLine("Too high!");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("You didn't follow the instructions!");
                }

            } while (!isNumberGuessed);

            Console.WriteLine("Your guesses were: ");
            guesses.ForEach(x => Console.WriteLine(x));
            Console.ReadLine();
        }

        public static bool ValidatedNumber(string strNumber)
        {
            int inputtedNumber;
            bool isANumber = int.TryParse(strNumber, out inputtedNumber);
            if (isANumber)
            {
                if (inputtedNumber < 1)
                {
                    return false;
                }
                else if (inputtedNumber > 20)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
