﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDrew
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();
            Console.WriteLine("Nice to meet you " + name +", what is your age?");
            var age = Console.ReadLine();
            Console.WriteLine("Your name is " + name + " and you're " + age + " years old!");
            Console.ReadLine();
            
        }
    }
}
