﻿using System;
using System.Linq;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            //PrintOutOfStock();
            //PrintInStockOver3();
            //PrintWashOrders();
            //PrintProductNames();
            PrintProductIncrease25();

            Console.ReadLine();
        }

         //Solution to #1
        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }
        //Solution to #2
        private static void PrintInStockOver3()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock > 0 && p.UnitPrice > 3);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }
        //Solution to #3
        private static void PrintWashOrders()
        {
            var customers = DataLoader.LoadCustomers();

            var results = customers.Where(c => c.Region == "WA");

            foreach (var customer in results)
            {
                Console.WriteLine(customer.CompanyName);
                foreach (Order order in customer.Orders)
                {
                    Console.WriteLine(order.OrderID + "\n" + order.OrderDate + "\n" + order.Total + "\n");
                }
            }
        }
        //Solution for #4
        private static void PrintProductNames()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Select(p => p.ProductName);

            foreach (var product in results)
            {
                Console.WriteLine(product);
            }
        }
        //Solution for #5
        private static void PrintProductIncrease25()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                select new {increase = p.ProductName + "\n" + (p.UnitPrice*1.25M) + "\n"};

            foreach (var product in results)
            {
                Console.WriteLine(product.increase);
            }

        }
    }
}
