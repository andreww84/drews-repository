﻿using System.Web.Mvc;
using MyContacts.UI.Models;

namespace MyContacts.UI.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            var database = new FakeContactDatabase();

            // ask the database to get all the contacts
            var contacts = database.GetAll();

            // inject the contact data into the view
            return View(contacts);
        }

        public ActionResult Add()
        {
            var model = new Contact();
            return View(model);
        }

        [HttpPost]
        public ActionResult AddContact(Contact c)
        {
            // create our fake database
            var database = new FakeContactDatabase();

            // add the contact record to the database
            database.Add(c);

            // tell the browser to navigate to Home/Index
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var database = new FakeContactDatabase();
            var contact = database.GetById(id);

            // inject the contact object into the view
            return View(contact);
        }

        [HttpPost]
        public ActionResult EditContact(Contact c)
        {
            // create our fake database
            var database = new FakeContactDatabase();

            // send the edited contact record to the database
            database.Edit(c);

            // tell the browser to navigate to Home/Index
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteContact(int ContactId)
        {
            var database = new FakeContactDatabase();
            database.Delete(ContactId);

            var contacts = database.GetAll();
            return View("Index", contacts);
        }

        [HttpPost]
        public ActionResult AddNumber(PhoneNumber p)
        {
            // create our fake database
            var database = new FakeContactDatabase();

            // add the contact record to the database
            database.Add(p);

            // tell the browser to navigate to Home/Index
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteNumber(int PhoneNumberId)
        {
            var database = new FakeContactDatabase();
            database.Delete(PhoneNumberId);

            var numbers = database.GetAll();
            return View("Index", numbers);
        }
	}
}

