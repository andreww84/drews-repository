﻿using System.Collections.Generic;
using System.Linq;

namespace MyContacts.UI.Models
{
    public class FakeContactDatabase
    {
        private static List<Contact> _contacts = new List<Contact>();
        private static List<PhoneNumber> _numbers = new List<PhoneNumber>(); 

        public List<Contact> GetAll()
        {
            return _contacts;
        }

        public void Add(Contact contact)
        {
            if (_contacts.Any())
                contact.ContactId = _contacts.Max(c => c.ContactId) + 1;
            else
                contact.ContactId = 1;

            _contacts.Add(contact);
        }

        public void Delete(int id)
        {
            _contacts.RemoveAll(c => c.ContactId == id);
        }

        public void Edit(Contact contact)
        {
            Delete(contact.ContactId);
            _contacts.Add(contact);
        }


        public Contact GetById(int id)
        {
            return _contacts.First(c => c.ContactId == id);
        }

        public void AddNumber(PhoneNumber phone)
        {
            if (_numbers.Any())
                phone.PhoneNumberId = _numbers.Max(n => n.PhoneNumberId) + 1;
            else
                phone.PhoneNumberId = 1;

            _numbers.Add(phone);
        }

        public void DeleteNumber(int phoneId)
        {
            _numbers.RemoveAll(n => n.PhoneNumberId == phoneId);
        }

        internal void Add(PhoneNumber p)
        {
            throw new System.NotImplementedException();
        }
    }
}