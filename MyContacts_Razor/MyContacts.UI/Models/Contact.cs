﻿using System.Collections.Generic;
namespace MyContacts.UI.Models
{
    public class Contact
    {
        public object PhoneNumber;

        public Contact()
        {
            PhoneNumbers = new List<PhoneNumber>();
        }

        public int ContactId { get; set; }
        public string Name { get; set; }
        public List<PhoneNumber> PhoneNumbers { get; set; }
        public bool IsFriend { get; set; }
    }
}

