﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _Test_AnswerCell
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            bool result = AnswerCell.Answer(false, false, false);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void TestMethod2()
        {
            bool result = AnswerCell.Answer(false, false, true);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void TestMethod3()
        {
            bool result = AnswerCell.Answer(true, false, false);
            Assert.AreEqual(false, result);
        }
    }
}
