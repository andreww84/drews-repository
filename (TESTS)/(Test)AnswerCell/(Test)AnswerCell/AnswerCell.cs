﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Test_AnswerCell
{
    class AnswerCell
    {
        public static bool Answer(bool isMorning, bool isMom, bool isAsleep)
        {
            if (isAsleep == true)
            {
                return false;
            }
            else if (isMorning == true && isMom == true && isAsleep == false)
            {
                return true;
            }
            else if (isMorning == true && isMom == false && isAsleep == false)
            {
                return false;
            }
            else return true;
        }
    }
}
