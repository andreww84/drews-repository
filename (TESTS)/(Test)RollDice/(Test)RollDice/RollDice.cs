﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Test_RollDice
{
    class RollDice
    {
        public static int Roll(int die1, int die2, bool noDoubles)
        {
                if ((die1 < die2) || (die1 > die2) || (die1 == die2) && noDoubles == false)
                {
                    return die1 + die2;
                }
                else if (die1 == die2 && noDoubles == true)
                {
                    if (die1 == 6)
                    {
                        die1 = 1;
                    }
                    else if (die2 == 6)
                    {
                        die2 = 1;
                    }
                    die1 += 1;
                }
                return die1 + die2;
        }
    }

//Return the sum of two 6-sided dice rolls, each in the range 1..6. However, if noDoubles is true, if the two dice show the same value, increment one die to the next value, wrapping around to 1 if its value was 6. 

//RollDice(2, 3, true) → 5
//RollDice(3, 3, true) → 7
//RollDice(3, 3, false) → 6

//public int RollDice(int die1, int die2, bool noDoubles) {

//}
}
