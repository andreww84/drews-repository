﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _Test_RollDice
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            int result = RollDice.Roll(2, 3, true);
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void TestMethod2()
        {
            int result = RollDice.Roll(3, 3, true);
            Assert.AreEqual(7, result);
        }

        [TestMethod]
        public void TestMethod3()
        {
            int result = RollDice.Roll(3, 3, false);
            Assert.AreEqual(6, result);
        }
    }
}
