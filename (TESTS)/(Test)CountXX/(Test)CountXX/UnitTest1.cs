﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _Test_CountXX
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            int result = CountXX.CountX("abcxx");
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void TestMethod2()
        {
            int result = CountXX.CountX("xxx");
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void TestMethod3()
        {
            int result = CountXX.CountX("xxxx");
            Assert.AreEqual(3, result);
        }
    }
}
