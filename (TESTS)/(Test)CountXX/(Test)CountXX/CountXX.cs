﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Test_CountXX
{
    class CountXX
    {
        public static int CountX(string str)
        {
            var count = 0;
            for (int i = 0; i < str.Length; i++)
                if (str.Substring(i, 1)== "x")
                {
                    count++;
                }
            return count - 1;
        }
    }

//Count the number of "xx" in the given string. We'll say that overlapping is allowed, so "xxx" contains 2 "xx". 

//CountXX("abcxx") -> 1
//CountXX("xxx") -> 2
//CountXX("xxxx") -> 3

//public int CountXX(string str) {

//}
}
