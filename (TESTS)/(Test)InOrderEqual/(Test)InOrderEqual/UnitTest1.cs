﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _Test_InOrderEqual
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            bool result = InOrderEqual.InOrder(2, 5, 11, false);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void TestMethod2()
        {
            bool result = InOrderEqual.InOrder(5, 7, 6, false);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void TestMethod3()
        {
            bool result = InOrderEqual.InOrder(5, 5, 7, true);
            Assert.AreEqual(true, result);
        }
    }
}
