﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Test_InOrderEqual
{
    class InOrderEqual
    {
        public static bool InOrder(int a, int b, int c, bool equalOk)
        {
            if (a < b && b < c && equalOk == false)
            {
                return true;
            }
            else if (a <= b && b <= c && equalOk == true)
            {
                return true;
            }
            return false;
        }
    }
}
