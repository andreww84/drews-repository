﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestFirstHalf
{
    class FirstHalfSolution
    {
        public string FirstHalf(string str)
        {
            string returnStr = "";
            if (str.Length%2 == 0)
            {
                returnStr = str.Substring(0, str.Length / 2);
            }
            return returnStr;
        }
    }
}
