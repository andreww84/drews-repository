﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _Test_Mod35
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            bool result = Mod35.Mod(3);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void TestMethod2()
        {
            bool result = Mod35.Mod(10);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void TestMethod3()
        {
            bool result = Mod35.Mod(15);
            Assert.AreEqual(false, result);
        }
    }
}
