﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Test_Mod35
{
    class Mod35
    {
        public static bool Mod(int n)
        {
            if (!((n > 0 && n % 3 == 0) && (n > 0 && n % 5 == 0)))
            {
                return true;
            }
            return false;
        }
    }
}
