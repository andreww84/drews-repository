﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestDemo
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            MathUtils mathUtils = new MathUtils();
            int result = mathUtils.Add(2, 2);
            Assert.AreEqual(4, result);
        }

        [TestMethod]
        public void TestSubtractMathUtils()
        {
            MathUtils mathUtils = new MathUtils();
            int result = mathUtils.Subtract(5, 3);
            Assert.AreEqual(2, result);
        }

    }
}
