﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _Test_LastDigit
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            bool result = LastDigit.LastDig(23, 19, 13);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void TestMethod2()
        {
            bool result = LastDigit.LastDig(23, 19, 12);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void TestMethod3()
        {
            bool result = LastDigit.LastDig(23, 19, 3);
            Assert.AreEqual(true, result);
        }
    }
}
