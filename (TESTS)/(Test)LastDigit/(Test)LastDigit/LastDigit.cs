﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Test_LastDigit
{
    class LastDigit
    {
        public static bool LastDig(int a, int b, int c)
        {
            var A = a.ToString();
            var B = b.ToString();
            var C = c.ToString();

            if ((A.Substring(A.Length - 1, 1) == B.Substring(B.Length - 1, 1)) || (B.Substring(B.Length - 1, 1) == C.Substring(C.Length - 1, 1)))
            {
                return true;
            }
            else if (A.Substring(A.Length - 1, 1) == C.Substring(C.Length - 1, 1))
            {
                return true;
            }
            return false;
        }
    }

//Given three ints, a b c, return true if two or more of them have the same rightmost digit. The ints are non-negative. 

//LastDigit(23, 19, 13) → true
//LastDigit(23, 19, 12) → false
//LastDigit(23, 19, 3) → true

//public bool LastDigit(int a, int b, int c)
//{

//}
}
