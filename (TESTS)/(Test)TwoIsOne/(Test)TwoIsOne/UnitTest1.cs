﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _Test_TwoIsOne
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            bool result = TwoIsOne.TwoOne(1, 2, 3);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void TestMethod2()
        {
            bool result = TwoIsOne.TwoOne(3, 1, 2);
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void TestMethod3()
        {
            bool result = TwoIsOne.TwoOne(3, 2, 2);
            Assert.AreEqual(false, result);
        }
    }
}
