﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Test_TwoIsOne
{
    class TwoIsOne
    {
        public static bool TwoOne(int a, int b, int c)
        {
            if ((a + b == c) || (b + c == a) || (c + a == b))
            {
                return true;
            }
            return false;
        }
    }
//Given three ints, a b c, return true if it is possible to add two of the ints to get the third. 

//TwoIsOne(1, 2, 3) → true
//TwoIsOne(3, 1, 2) → true
//TwoIsOne(3, 2, 2) → false

//public bool TwoIsOne(int a, int b, int c) {
  
//}
}
