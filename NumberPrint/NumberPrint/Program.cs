﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberPrint
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number and I will convert it to currency format.");
            var input = Console.ReadLine();
            //Console.WriteLine("$" + input + ".00");
            //Console.ReadLine();
            var num = Int32.Parse(input);
            Console.WriteLine(string.Format("{0:C}", num));
            Console.ReadLine();
        }
    }
}
