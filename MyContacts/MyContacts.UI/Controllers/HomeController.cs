﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using MyContacts.UI.Models;

namespace MyContacts.UI.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            var database = new FakeContactDatabase();

            // ask the database to get all the contacts
            var contacts = database.GetAll();

            // inject the contact data into the view
            return View(contacts);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddContact()
        {
            // create a contact
            var c = new Contact();

            // get the data from the input fields Name and PhoneNumber
            c.Name = Request.Form["Name"];
            //c.PhoneNumber = Request.Form["PhoneNumber"];

            if (Request.Form["personal"] == "value")
            {
                c.IsPersonal = true;
            }
            else
            {
                c.IsPersonal = false;
            }

            if (Request.Form["professional"] == "value")
            {
                c.IsProfessional = true;
            }
            else
            {
                c.IsProfessional = false;
            }
            
            // create our fake database
            var database = new FakeContactDatabase();

            // add the contact record to the database
            database.Add(c);

            // tell the browser to navigate to Home/Index
            return RedirectToAction("Index");
        }

        public ActionResult Edit()
        {
            // Load the id from the RouteData.  
            // It is stored as object, so we must cast it.
            int contactId = int.Parse(RouteData.Values["id"].ToString());
            var database = new FakeContactDatabase();
            var contact = database.GetById(contactId);

            // inject the contact object into the view
            return View(contact);

        }

        [HttpPost]
        public ActionResult EditContact()
        {
            // create a contact
            var c = new Contact();

            // get the data from the input fields 
            // ContactId, Name, and PhoneNumber
            c.Name = Request.Form["Name"];
            //c.PhoneNumber = Request.Form["PhoneNumber"];
            c.ContactId = int.Parse(Request.Form["ContactId"]);
            if (Request.Form["personal"] == "value")
            {
                c.IsPersonal = true;
            }
            else
            {
                c.IsPersonal = false;
            }

            if (Request.Form["professional"] == "value")
            {
                c.IsProfessional = true;
            }
            else
            {
                c.IsProfessional = false;
            }
            // create our fake database
            var database = new FakeContactDatabase();

            // send the edited contact record to the database
            database.Edit(c);

            // tell the browser to navigate to Home/Index
            return RedirectToAction("Index");

        }

        [HttpPost]
        public ActionResult DeleteContact()
        {
            int contactId = int.Parse(Request.Form["ContactId"]);

            var database = new FakeContactDatabase();
            database.Delete(contactId);

            var contacts = database.GetAll();
            return View("Index", contacts);
        }

        [HttpPost]
        public ViewResult Partial()
        {
            return View("Partial");
        }
    }
}

