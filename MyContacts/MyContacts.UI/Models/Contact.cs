﻿
using System.Collections.Generic;

namespace MyContacts.UI.Models
{
    public class Contact
    {
        public int ContactId { get; set; }
        public string Name { get; set; }
        public List<PhoneNumber> PhoneNumbers { get; set; }
        public bool IsPersonal { get; set; }
        public bool IsProfessional { get; set; }
    }
}

