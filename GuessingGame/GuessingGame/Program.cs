﻿using System;

namespace GuessingGame
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();
            int theAnswer;
            int playerGuess;
            string playerInput;
            bool isNumberGuessed = false;
            var counter = 0;

            Random r = new Random(); // This is System's built in random number generator
            theAnswer = r.Next(1, 21); // Get a random from 1 to 20

 

            do
            {              
                // get player input
                Console.Write(name + " Enter your guess (any number between 1 and 20) or press 'q' at any time to quit.");
                playerInput = Console.ReadLine();
                counter++;
                if (playerInput == "q")
                {
                    break;
                }
                
                // attempt to convert to number
                if (int.TryParse(playerInput, out playerGuess))
                {
                    if (playerGuess < 1 || playerGuess > 20)
                    {
                        Console.WriteLine(name + ", your guess needs to be between 1 and 20!");
                    }

                    if(playerGuess == theAnswer && counter == 1)
                    {
                        Console.WriteLine("Great job " + name + "! " + "You guessed it on the first try!");
                    }
                    // see if they won
                    if (playerGuess == theAnswer)
                    {
                        Console.WriteLine("You got it " + name);
                        isNumberGuessed = true;
                    }

                    else
                    {
                        // they didn't win, so were they too high or too low?
                        if (playerGuess > theAnswer)
                            Console.WriteLine("Too high!");
                        else
                            Console.WriteLine("Too low!");
                    }
                }
                else
                {
                    // learn to play n00b
                    Console.WriteLine("That wasn't a valid number!");
                }
            } while (!isNumberGuessed);
            
            Console.WriteLine(name + ", Press any key to quit.");
            Console.ReadLine();
        }
    }
}
