﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMSMastery.Models;

namespace LMSMastery.Repositories
{
    interface IAdminRepository
    {
        List<RegisterViewModel> GetUnassignedUsers();
        Task ConfirmUnassignedUser(ApplicationUser user, RegisterViewModel model);
        RegisterViewModel UserDetails(int id);
        void EdidLmsUserId(RegisterViewModel model);
        List<RegisterViewModel> Search(string lastName, string firstName, string email, string role);        
        //void DeleteUser(int userId);
    }
}
