﻿using LMSMastery.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Configuration;
using System.Threading.Tasks;
using System.Text;

namespace LMSMastery.Repositories
{
    public class DBAdminRepository : IAdminRepository
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["SWC_LMS"].ConnectionString;
        private ApplicationUserManager _userManager;



        public List<RegisterViewModel> GetUnassignedUsers()
        {
            List<RegisterViewModel> userList = new List<RegisterViewModel>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("SELECT UserId, LastName, FirstName, Email, SuggestedRole FROM LmsUser WHERE Id IS NULL", conn);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    RegisterViewModel lmsUser = new RegisterViewModel();
                    lmsUser.UserId = reader.GetInt32(0);
                    lmsUser.LastName = reader.GetString(1);
                    lmsUser.FirstName = reader.GetString(2);
                    lmsUser.Email = reader.GetString(3).ToString();
                    lmsUser.SuggestedRole = reader.GetString(4);
                    userList.Add(lmsUser);
                }
            }
            return userList;
        }

        public async Task ConfirmUnassignedUser(ApplicationUser user, RegisterViewModel model)
        {
            var result = await _userManager.CreateAsync(user, model.Password);
            EdidLmsUserId(model);
        }

        public RegisterViewModel UserDetails(int id)
        {
            RegisterViewModel details = new RegisterViewModel();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("SELECT UserId, LastName, FirstName, GradeLevelId, SuggestedRole FROM LmsUser WHERE UserId = " + id, conn);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {                    
                    details.UserId = reader.GetInt32(0);
                    details.LastName = reader.GetString(1);
                    details.FirstName = reader.GetString(2);
                    details.GradeLevel = reader.GetByte(3);
                    details.SuggestedRole = reader.GetString(4);
                }
            }
            return details;
        }



        public void EdidLmsUserId(RegisterViewModel model)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var commandText = "UPDATE LmsUser SET Id = AspNetUsers WHERE Id = @Id";
                SqlCommand command = new SqlCommand(commandText, conn);                

                command.Connection.Open();
                command.ExecuteNonQuery();
            }
        }

        public List<RegisterViewModel> Search(string lastName, string firstName, string email, string role)
        {
            var builder = new List<string>();
            var query = new StringBuilder();
            
            if (!string.IsNullOrEmpty(lastName))
            {
                builder.Add(string.Format(" LastName LIKE '%{0}%' ", lastName));
            }

            if (!string.IsNullOrEmpty(firstName))
            {
                builder.Add(string.Format(" FirstName LIKE '%{0}%' ", firstName));
            }

            if (!string.IsNullOrEmpty(email))
            {
                builder.Add(string.Format(" Email LIKE '%{0}%' ", email));
            }

            if (!string.IsNullOrEmpty(role))
            {
                builder.Add(string.Format(" SuggestedRole = '{0}' ", role));
            }

            query.AppendFormat("SELECT UserId, LastName, FirstName, Email, SuggestedRole FROM LmsUser WHERE {0}", string.Join("OR", builder.ToArray()));
            List<RegisterViewModel> userDetails = new List<RegisterViewModel>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query.ToString(), conn);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    RegisterViewModel details = new RegisterViewModel();
                    details.UserId = reader.GetInt32(0);
                    details.LastName = reader.GetString(1);
                    details.FirstName = reader.GetString(2);
                    details.Email = reader.GetString(3);
                    details.SuggestedRole = reader.GetString(4);
                    userDetails.Add(details);
                }
            }

            return userDetails;
        }       

        

        //public void DeleteUser(int id)
        //{
        //    using (SqlConnection conn = new SqlConnection(connectionString))
        //    {
        //        var commandText = "DELETE FROM ToDo WHERE Id = @Id";
        //        SqlCommand command = new SqlCommand(commandText, conn);
        //        command.Parameters.AddWithValue("@Id", id);

        //        command.Connection.Open();
        //        command.ExecuteNonQuery();
        //    }
        //}

        

        //public Models.User GetUserByFirstName(string first)
        //{
        //    return GetUserByFirstName().FirstName(x => x.FirstName = first);
        //}

        //public Models.User GetUserByEmail(string email)
        //{
        //    return GetUserByEmail().Email(x => x.Email == email);
        //}

        //public Models.User GetUserByRole(string role)
        //{
        //    return GetUserByRole().SuggestedRole(x => x.SuggestedRole == role);
        //}

    }

}