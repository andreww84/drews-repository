﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMSMastery.Models;

namespace LMSMastery.Repositories
{
    interface IUserRepository
    {
        void AddNewLmsUser(RegisterViewModel user);
        List<RegisterViewModel> GetStudentRoster(int rosterId);
        void AddStudent(RegisterViewModel student);
        List<RegisterViewModel> AddStudentSearch(string lastName, string role);
        void AddNewClass(Course course);
    }
}
