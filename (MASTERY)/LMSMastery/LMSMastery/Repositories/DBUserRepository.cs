﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LMSMastery.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;

namespace LMSMastery.Repositories
{
    public class DBUserRepository : IUserRepository
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["SWC_LMS"].ConnectionString;

        public void AddNewLmsUser(RegisterViewModel user)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var commandText = "INSERT INTO LmsUser (FirstName, LastName, Email, SuggestedRole, GradeLevelId) VALUES (@FirstName, @LastName, @Email, @SuggestedRole, @GradeLevelId)";
                SqlCommand command = new SqlCommand(commandText, conn);
                command.Parameters.AddWithValue("@FirstName", user.FirstName);
                command.Parameters.AddWithValue("@LastName", user.LastName);
                command.Parameters.AddWithValue("@Email", user.Email);
                command.Parameters.AddWithValue("@SuggestedRole", user.SuggestedRole);
                command.Parameters.AddWithValue("@GradeLevelId", user.GradeLevel);

                command.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        public List<RegisterViewModel> GetStudentRoster(int rosterId)
        {
            List<RegisterViewModel> classRoster = new List<RegisterViewModel>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("SELECT UserId, FirstName, LastName, Email FROM LmsUser INNER JOIN Roster ON LmsUser.UserId = Roster.UserId WHERE RosterId = " + rosterId, conn);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    RegisterViewModel lmsUser = new RegisterViewModel();
                    lmsUser.UserId = reader.GetInt32(0);
                    lmsUser.FirstName = reader.GetString(1);
                    lmsUser.LastName = reader.GetString(2);
                    lmsUser.Email = reader.GetString(3).ToString();
                    classRoster.Add(lmsUser);
                }
            }
            return classRoster;
        }

        public void AddStudent(RegisterViewModel student)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var commandText = "INSERT INTO Roster (UserId) VALUES (@UserId)";
                SqlCommand command = new SqlCommand(commandText, conn);
                command.Parameters.AddWithValue("@UserId", student.UserId);

                command.Connection.Open();
                command.ExecuteNonQuery();

            }
        }

        public List<RegisterViewModel> AddStudentSearch(string lastName, string role)
        {
            var builder = new List<string>();
            var query = new StringBuilder();

            if (!string.IsNullOrEmpty(lastName))
            {
                builder.Add(string.Format(" LastName LIKE '%{0}%' ", lastName));
            }

            if (!string.IsNullOrEmpty(role))
            {
                builder.Add(string.Format(" SuggestedRole = '{0}' ", role));
            }

            query.AppendFormat("SELECT UserId, LastName, SuggestedRole FROM LmsUser WHERE {0}", string.Join("OR", builder.ToArray()));
            List<RegisterViewModel> userDetails = new List<RegisterViewModel>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query.ToString(), conn);
                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    RegisterViewModel details = new RegisterViewModel();
                    details.UserId = reader.GetInt32(0);
                    details.LastName = reader.GetString(1);
                    details.SuggestedRole = reader.GetString(2);
                    userDetails.Add(details);
                }
            }

            return userDetails;
        }

        public void AddNewClass(Course course)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                var commandText = "INSERT INTO Course (SubjectId, CourseName, CourseDescription, GradeLevel, IsArchived, StartDate, EndDate) VALUES (@SubjectId, @CourseName, @CourseDescription, @GradeLevel, @IsArchived, @StartDate, @EndDate)";
                SqlCommand command = new SqlCommand(commandText, conn);
                command.Parameters.AddWithValue("@SubjectId", course.Subject);
                command.Parameters.AddWithValue("@CourseName", course.CourseName);
                command.Parameters.AddWithValue("@CourseDescription", course.CourseDescription);
                command.Parameters.AddWithValue("@GradeLevel", course.GradeLevel);
                command.Parameters.AddWithValue("@IsArchived", course.IsArchived);
                command.Parameters.AddWithValue("@StartDate", course.StartDate);
                command.Parameters.AddWithValue("@EndDate", course.EndDate);

                command.Connection.Open();
                command.ExecuteNonQuery();

            }
        }
    }
}