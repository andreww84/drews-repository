﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMSMastery.Models
{
    public class Roster
    {
        public int RosterId { get; set; }
        public string CurrentGrade { get; set; }
        public bool IsDeleted { get; set; }
    }
}