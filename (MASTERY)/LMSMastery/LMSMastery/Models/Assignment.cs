﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMSMastery.Models
{
    public class Assignment
    {
        public int AssignmentId { get; set; }
        public string AssignmentName { get; set; }
        public int PossiblePoints { get; set; }
        public DateTime DueDate { get; set; }
        public string Description { get; set; }

    }
}