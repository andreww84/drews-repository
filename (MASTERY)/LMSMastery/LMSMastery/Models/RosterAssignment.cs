﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMSMastery.Models
{
    public class RosterAssignment
    {
        public int RosterAssignmentId { get; set; }
        public int PonitsEarned { get; set; }
        public decimal Percentage { get; set; }
        public string Grade { get; set; }
    }
}