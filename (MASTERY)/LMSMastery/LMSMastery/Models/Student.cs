﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMSMastery.Models
{
    public class Student : User
    {
        public int GradeLevel { get; set; }
        public string Course { get; set; }
        public string CurrentGrade { get; set; }
        public int AssignmentScore { get; set; }
        public int QuizScore { get; set; }
        public int TestScore { get; set; }
    }
}