﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMSMastery.Models
{
    public class Teacher : User
    {
        public string MyClasses { get; set; }
        public int Current { get; set; }
        public int Archived { get; set; }
    }
}