﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LMSMastery.Models
{
    public class Course
    {
        public int CourseId {get; set;}
        public string CourseName { get; set; }
        public string Subject { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int GradeLevel { get; set; }
        public string CourseDescription { get; set; }
        public bool IsArchived { get; set; }
        public int TotalStudents { get; set; }
        public int ByGradeA { get; set; }
        public int ByGradeB { get; set; }
        public int ByGradeC { get; set; }
        public int ByGradeD { get; set; }
        public int ByGradeF { get; set; }
    }
}