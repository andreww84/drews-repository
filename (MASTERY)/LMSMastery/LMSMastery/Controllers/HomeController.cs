﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using LMSMastery.Repositories;

namespace LMSMastery.Controllers
{
    public class HomeController : Controller
    {
        private IUserRepository _userRepo = new DBUserRepository();


        public ActionResult Index()
        {           
            return View();
        }

        public ActionResult PartialRegister()
        {
            return View();
        }

        public ActionResult Registered()
        {
            return View();
        }
    }
}