﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMSMastery.Controllers
{
    public class ParentController : Controller
    {
        public ActionResult ParentDashboard()
        {
            return View();
        }
    }
}