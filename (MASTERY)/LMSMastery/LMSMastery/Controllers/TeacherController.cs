﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMSMastery.Controllers
{
    public class TeacherController : Controller
    {
        public ActionResult TeacherDashboard()
        {
            return View();
        }

        //public ActionResult ClassDetails()
        //{
        //    return View();
        //}

        public ActionResult Roster()
        {
            return View();
        }

        public ActionResult Gradebook()
        {
            return View();
        }

        public ActionResult AddAssignment()
        {
            return View();
        }
    }
}