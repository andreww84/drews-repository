﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LMSMastery.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult AdminDashboard()
        {
            return View();
        }

        public ActionResult UserDetails()
        {
            return View();
        }

        public ActionResult UserSearch()
        {
            return View();
        }
        
    }
}