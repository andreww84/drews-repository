﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FlooringProgram.Models
{
    public class Order
    {
        private IEnumerable<Order> ord;
        
        public Order(IEnumerable<Order> ord)
        {
            this.ord = ord;
        }

        public Order()
        {

        }
        public string OrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string State { get; set; }
        public decimal TaxRate { get; set; }
        public string ProductType { get; set; }
        public decimal Area { get; set; }
        public decimal CostPerSqFt { get; set; }
        public decimal LaborPerSqFt { get; set; }
        public decimal MaterialCost { get; set; }
        public decimal TotalLaborCost { get; set; }
        public decimal TotalTax { get; set; }
        public decimal OrderTotal { get; set; }

        public string Detail
        {
            get
            {
                var sb = new StringBuilder();

                sb.AppendLine(String.Format("Order Number: {0}", OrderNumber));
                sb.AppendLine(String.Format("Customer Name: {0}", CustomerName));
                sb.AppendLine(String.Format("State: {0}", State));
                sb.AppendLine(String.Format("Tax Rate: {0}%", TaxRate));
                sb.AppendLine(String.Format("Product Type: {0}", ProductType));
                sb.AppendLine(String.Format("Area: {0}", Area));
                sb.AppendLine(String.Format("Cost / sq ft: {0:C}/ sq ft", CostPerSqFt));
                sb.AppendLine(String.Format("Labor / sq ft: {0:C} /sq ft", LaborPerSqFt));
                sb.AppendLine(String.Format("Material Cost: {0:C}", MaterialCost));
                sb.AppendLine(String.Format("Total Labor Cost: {0:C}", TotalLaborCost));
                sb.AppendLine(String.Format("Tax: {0:C}", TotalTax));
                sb.AppendLine(String.Format("Order Total: {0:C}", OrderTotal));

                return sb.ToString();
            }
        }
    }
}
