﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;

namespace FlooringProgram.Models
{
    public class OrderEntry
    {
        public OrderEntry()
        {
            OrderList = new List<Order>();
        }

        //public string Name { get; set; }
        public List<Order> OrderList { get; set; }
        public string Date { get; set; }

         
    }
}
