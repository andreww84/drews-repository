﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Data.TaxRates;
using FlooringProgram.Models;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Operations
{
    public class ProductOperations
    {
        public static bool IsAllowedInfo(string info)
        {
            IProductRepository repository = ProductRepositoryFactory.GetProductRepository();
            var allProductInfo = repository.GetProductInfo();

            return allProductInfo.Any(t => t.Type == info || t.CostPerSqFt.ToString() == info || t.LaborCostPerSqFt.ToString() == info);
        }

        public static Product GetProductInfoFor(string info)
        {
            IProductRepository repository = ProductRepositoryFactory.GetProductRepository();
            var allProductInfo = repository.GetProductInfo();

            return allProductInfo.FirstOrDefault(t => t.Type == info || t.CostPerSqFt.ToString() == info || t.LaborCostPerSqFt.ToString() == info);
        }
    }
}

