﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;
using FlooringProgram.Data;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Data.TaxRates;
using FlooringProgram.Models.Interfaces;
using FlooringProgram.Operations;

namespace FlooringProgram.UI
{
    public class Program
    {
        public static IOrderRepository _orderRepository = OrderFileFactory.GetOrderRepository();
        private static string _fileTemplate = @"Data\Orders_{0}.txt";
        private static OrderEntry _orders;
        public static TaxRateFileRepository _getTax;

        private static void Main(string[] args)
        {
            if (ConfigurationSettings.GetMode() == "Test")
            {

            }

            string userSelectedOption = "";
            var today = DateTime.Today;
            var date = today.ToString("MMddyyyy");
            var file = String.Format(_fileTemplate, date);
            _orders = _orderRepository.Load(file);
            _orders.Date = date;

            do
            {
                userSelectedOption = GetUserSelectedOption();
                switch (userSelectedOption)
                {
                    case "1":
                        DisplayOrders();
                        break;
                    case "2":
                        AddOrder();
                        break;
                    case "3":
                        EditOrder();
                        break;
                    case "4":
                        RemoveOrder();
                        break;
                    case "5":
                        break;
                    default:
                        Console.WriteLine("That is not a valid option");
                        Console.WriteLine("");
                        break;
                }
            } while (userSelectedOption != "5");
        }


        //  Console.Write("Enter a state: ");
        //    string state = Console.ReadLine();

        //    TaxRateOperations taxOps = new TaxRateOperations();
        //    if (taxOps.IsAllowedState(state))
        //    {
        //        Console.WriteLine("That is a valid state");
        //        TaxRate rate = taxOps.GetTaxRateFor(state);

        //        Console.WriteLine("The tax rate for {0} is {1:p}", rate.State, rate.TaxPercent);
        //    }
        //    else
        //    {
        //        Console.WriteLine("That is not a valid state");
        //    }

        //    Console.ReadLine();


        private static void DisplayOrders()
        {
            Console.Write("Enter an order date: ");
            var date = Console.ReadLine();
            string FileName = String.Format(_fileTemplate, date);
            _orders = _orderRepository.Load(FileName);
            _orders.Date = date;

            if (_orders.OrderList.Any())
            {
                Console.WriteLine("Order data for the date {0}", date);
                foreach (var o in _orders.OrderList)
                {
                    Console.WriteLine(o.Detail);
                }
            }
            else
            {
                Console.WriteLine("No data found for the date {0}", date);
            }
        }

        private static void AddOrder()
        {       
            var getTax = new TaxRateOperations();
            var newOrder = new Order();
            Console.Write("What is the Customer's Name: ");
            newOrder.CustomerName = Console.ReadLine();
            Console.Write("What is the state: ");
            newOrder.State = Console.ReadLine();
            var isValidState = TaxRateOperations.IsAllowedState(newOrder.State);

            if (!isValidState)
            {
                Console.WriteLine("That was not a valid State");
                return;
            }
            newOrder.TaxRate = TaxRateOperations.GetTaxRateFor(newOrder.State).TaxPercent;
            Console.Write("What type of product: ");
            newOrder.ProductType = Console.ReadLine();
            var isValidProduct = ProductOperations.IsAllowedInfo(newOrder.ProductType);

            if (!isValidProduct)
            {
                Console.WriteLine("That was not a valid Product");
                return;
            }
            newOrder.CostPerSqFt = ProductOperations.GetProductInfoFor(newOrder.ProductType).CostPerSqFt;
            newOrder.LaborPerSqFt = ProductOperations.GetProductInfoFor(newOrder.ProductType).LaborCostPerSqFt;
            Console.Write("What is the area of the project: ");
            newOrder.Area = decimal.Parse(Console.ReadLine());
            newOrder.MaterialCost = newOrder.CostPerSqFt*newOrder.Area;
            newOrder.TotalLaborCost = newOrder.LaborPerSqFt*newOrder.Area;
            newOrder.TotalTax = ((newOrder.TaxRate/100)*(newOrder.MaterialCost + newOrder.TotalLaborCost));
            newOrder.OrderTotal = newOrder.MaterialCost + newOrder.TotalLaborCost + newOrder.TotalTax;

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Order Details: ");
            Console.WriteLine(newOrder.Detail);

            Console.Write("Do you want to save this data (y/n)");
            var sords = Console.ReadLine().ToLower();         
            if (sords == "y")
            {
                newOrder.OrderNumber = ((_orders.OrderList.Count) + 1).ToString();
                _orders.OrderList.Add(newOrder);
                _orderRepository.Save(String.Format(_fileTemplate, _orders.Date), _orders);
                Console.WriteLine("Order saved successfully!");
            }
            else
            {
                Console.WriteLine("Order info was discarded!");
            }

        }

        private static void EditOrder()
        {
            Console.Write("Specify date : ");
            var date = Console.ReadLine();
            Console.Write("Specify Order Number to edit : ");
            var num = Console.ReadLine();

            string FileName = String.Format(_fileTemplate, date);
            _orders = _orderRepository.Load(FileName);
            _orders.Date = date;

            if (_orders.OrderList.Any(x => x.OrderNumber.Equals(num, StringComparison.InvariantCultureIgnoreCase)))
            {
                var ord = from o in _orders.OrderList
                    where o.OrderNumber == num
                    select o;


                foreach (var n in ord)
                {
                    Console.WriteLine("Order details for order number {0}", num);
                    Console.WriteLine();
                    Console.WriteLine(n.Detail);
                    Console.WriteLine();
                    Console.WriteLine();

                }
                Console.Write("Are you sure you want to edit this order (y/n): ");
                if (Console.ReadLine().Equals("y", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (_orders.OrderList.Where(a => a.OrderNumber.Contains(num)) != null)
                    {
                        var getTax = new TaxRateOperations();
                        var newOrder = new Order(ord);
                        Console.Write("What is the Customer's Name: ");
                        newOrder.CustomerName = Console.ReadLine();
                        Console.Write("What is the state: ");
                        newOrder.State = Console.ReadLine();
                        var isValidState = TaxRateOperations.IsAllowedState(newOrder.State);

                        if (!isValidState)
                        {
                            Console.WriteLine("That was not a valid State");
                            return;
                        }
                        newOrder.TaxRate = TaxRateOperations.GetTaxRateFor(newOrder.State).TaxPercent;
                        Console.Write("What type of product: ");
                        newOrder.ProductType = Console.ReadLine();
                        var isValidProduct = ProductOperations.IsAllowedInfo(newOrder.ProductType);

                        if (!isValidProduct)
                        {
                            Console.WriteLine("That was not a valid Product");
                            return;
                        }
                        newOrder.CostPerSqFt = ProductOperations.GetProductInfoFor(newOrder.ProductType).CostPerSqFt;
                        newOrder.LaborPerSqFt =
                        newOrder.LaborPerSqFt = ProductOperations.GetProductInfoFor(newOrder.ProductType).LaborCostPerSqFt;
                        Console.Write("What is the area of the project: ");
                        newOrder.Area = decimal.Parse(Console.ReadLine());
                        newOrder.MaterialCost = newOrder.CostPerSqFt * newOrder.Area;
                        newOrder.TotalLaborCost = newOrder.LaborPerSqFt * newOrder.Area;
                        newOrder.TotalTax = ((newOrder.TaxRate / 100) * (newOrder.MaterialCost + newOrder.TotalLaborCost));
                        newOrder.OrderTotal = newOrder.MaterialCost + newOrder.TotalLaborCost + newOrder.TotalTax;

                        if (newOrder.Equals(ord))
                        {
                            Console.WriteLine("No changes were detected to order number {0}", num);
                            Console.WriteLine();
                        }
                        else
                        {
                            Console.WriteLine("New Order Details: ");
                            Console.WriteLine(newOrder.Detail);
                        }
                        Console.Write("Do you want to save the new changes? (y/n)");
                        var sords = Console.ReadLine().ToLower();
                        if (sords == "y")
                        {
                            _orders.OrderList.Remove(
                               _orders.OrderList.FirstOrDefault(
                                    x => x.OrderNumber.Equals(num, StringComparison.InvariantCultureIgnoreCase)));
                            newOrder.OrderNumber = num;
                            _orders.OrderList.Add(newOrder);
                            _orderRepository.Save(String.Format(_fileTemplate, _orders.Date), _orders);
                            Console.WriteLine("Order saved successfully!");
                        }
                        else
                        {
                            Console.WriteLine("Order info was discarded!");
                        }

                    }
                }
                else
                {
                    Console.WriteLine("No changes were saved to order number {0}", num);
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("Order number {0} does not exist.", num);
            }

        }


        private static void RemoveOrder()
        {
            Console.Write("Specify date : ");
            var date = Console.ReadLine();
            Console.Write("Specify Order Number : ");
            var num = Console.ReadLine();
            Console.WriteLine();
            Console.WriteLine();

            string FileName = String.Format(_fileTemplate, date);
            _orders = _orderRepository.Load(FileName);
            _orders.Date = date;

            if (_orders.OrderList.Any(x => x.OrderNumber.Equals(num, StringComparison.InvariantCultureIgnoreCase)))
            {
                var ord = from o in _orders.OrderList
                    where o.OrderNumber == num
                    select o;

                foreach (var n in ord)
                {
                    Console.WriteLine("Order details for order number {0}", num);
                    Console.WriteLine();
                    Console.WriteLine(n.Detail);
                    Console.WriteLine();
                }
                Console.Write("Are you sure you want to remove order number {0}? (y/n)", num);

                if (Console.ReadLine().Equals("y", StringComparison.InvariantCultureIgnoreCase))
                {
                    _orders.OrderList.Remove(
                        _orders.OrderList.FirstOrDefault(
                            x => x.OrderNumber.Equals(num, StringComparison.InvariantCultureIgnoreCase)));
                    _orderRepository.Save(String.Format(_fileTemplate, _orders.Date), _orders);
                    Console.WriteLine();
                    Console.WriteLine("Order removed successfully!");
                    Console.WriteLine();
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("Order number {0} does not exist.", num);
            }
            // var orders = _orderRepository.Load()

        }

        private static string GetUserSelectedOption()
        {
            Console.WriteLine("**************************");
            Console.WriteLine("*    Flooring Program    *");
            Console.WriteLine("*                        *");
            Console.WriteLine("*  1.) Display Orders    *");
            Console.WriteLine("*  2.) Add an Order      *");
            Console.WriteLine("*  3.) Edit an Order     *");
            Console.WriteLine("*  4.) Remove an Order   *");
            Console.WriteLine("*  5.) Quit              *");
            Console.WriteLine("**************************");
            Console.WriteLine();
            Console.Write("Please choose what you would like to do: ");
            string userChoice = Console.ReadLine();

            return userChoice;
        }
    }
}




