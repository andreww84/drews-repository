﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.Data
{
    public class OrderFileRepository : IOrderRepository
    {
        public OrderEntry Load(string fileName)
        {
            OrderEntry orders = new OrderEntry();

            if (File.Exists(fileName))
            {
                try
                {
                    using (StreamReader reader = new StreamReader(fileName))
                {
                    string line = reader.ReadLine();
                    while (line != null)
                        
                    {
                        Order newOrder = new Order();

                        string[] properties = line.Split(',');

                        newOrder.OrderNumber = properties[0];
                        newOrder.CustomerName = properties[1];
                        newOrder.State = properties[2];
                        newOrder.TaxRate = decimal.Parse(properties[3]);
                        newOrder.ProductType = properties[4];
                        newOrder.Area = decimal.Parse(properties[5]);
                        newOrder.CostPerSqFt = decimal.Parse(properties[6]);
                        newOrder.LaborPerSqFt = decimal.Parse(properties[7]);
                        newOrder.MaterialCost = decimal.Parse(properties[8]);
                        newOrder.TotalLaborCost = decimal.Parse(properties[9]);
                        newOrder.TotalTax = decimal.Parse(properties[10]);
                        newOrder.OrderTotal = decimal.Parse(properties[11]);

                        orders.OrderList.Add(newOrder);

                        line = reader.ReadLine();
                    }
                }
                }
                catch (Exception)
                {
                    Console.WriteLine("The selected file has been corrupted. Please select a new file.");
                }
            }
            
            return orders;
        }

        public void Save(string fileName, OrderEntry orders)
        {
            using (StreamWriter writer = new StreamWriter(fileName, false))
            {
                foreach (Order order in orders.OrderList)
                {
                    string[] fields = new string[12];

                    fields[0] = order.OrderNumber;
                    fields[1] = order.CustomerName;
                    fields[2] = order.State;
                    fields[3] = order.TaxRate.ToString() ;
                    fields[4] = order.ProductType;
                    fields[5] = order.Area.ToString();
                    fields[6] = order.CostPerSqFt.ToString();
                    fields[7] = order.LaborPerSqFt.ToString();
                    fields[8] = order.MaterialCost.ToString();
                    fields[9] = order.TotalLaborCost.ToString();
                    fields[10] = order.TotalTax.ToString();
                    fields[11] = order.OrderTotal.ToString();
                    string fieldsStr = string.Join(",", fields);

                    writer.WriteLine(fieldsStr);
                }
            }
        }

    }
}
