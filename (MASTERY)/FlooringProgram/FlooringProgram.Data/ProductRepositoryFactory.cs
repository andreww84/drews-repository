﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Data.TaxRates;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data
{
    public class ProductRepositoryFactory
    {
        public static IProductRepository GetProductRepository()
        {
            switch (ConfigurationSettings.GetMode())
            {
                case "Prod":
                    return new ProductRepository();
                case "Test":
                    return new ProductTestRepository();
            }

            return null;
        }
    }
}
