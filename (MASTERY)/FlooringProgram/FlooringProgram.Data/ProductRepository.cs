﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.Data
{
    public class ProductRepository : IProductRepository
    {

        public List<Models.Product> GetProductInfo()
        {
            List<Product> productInfo = new List<Product>();

            string[] info = File.ReadAllLines(@"Data\Products.txt");
            for (int i = 1; i < info.Length; i++)
            {
                string[] row = info[i].Split(',');

                Product toAdd = new Product();
                toAdd.Type = row[0];
                toAdd.CostPerSqFt = decimal.Parse(row[1]);
                toAdd.LaborCostPerSqFt = decimal.Parse(row[2]);

                productInfo.Add(toAdd);

            }
            return productInfo;
        }
    }
}
