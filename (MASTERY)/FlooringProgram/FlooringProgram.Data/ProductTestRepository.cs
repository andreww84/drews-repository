﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.Data
{
    public class ProductTestRepository : IProductRepository
    {

        public List<Models.Product> GetProductInfo()
        {
            return new List<Product>()
            {
                new Product() {Type = "Carpet", CostPerSqFt = 2.25M, LaborCostPerSqFt = 2.10M},
                new Product() {Type = "Laminate", CostPerSqFt = 1.75M, LaborCostPerSqFt = 2.10M},
                new Product() {Type = "Tile", CostPerSqFt = 3.50M, LaborCostPerSqFt = 4.15M},
                new Product() {Type = "Wood", CostPerSqFt = 5.15M, LaborCostPerSqFt = 4.75M}
            };
        }
    }
}
