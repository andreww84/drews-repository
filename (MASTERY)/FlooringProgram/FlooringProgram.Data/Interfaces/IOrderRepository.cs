﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Models;

namespace FlooringProgram.Data.Interfaces
{
    public interface IOrderRepository
    {
        OrderEntry Load(string fileName);
        void Save(string fileName, OrderEntry orders);
    }
}
