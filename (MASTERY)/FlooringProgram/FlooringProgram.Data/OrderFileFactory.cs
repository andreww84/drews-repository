﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models.Interfaces;

namespace FlooringProgram.Data
{
    public class OrderFileFactory
    {
        public static IOrderRepository GetOrderRepository()
        {
            switch (ConfigurationSettings.GetMode())
            {
                case "Prod":
                    return new OrderFileRepository();
                case "Test":
                    return new OrderFileTestRepository();
            }

            return null;
        }
    }
}
