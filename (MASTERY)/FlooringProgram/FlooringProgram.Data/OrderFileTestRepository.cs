﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringProgram.Data.Interfaces;
using FlooringProgram.Models;

namespace FlooringProgram.Data
{
    public class OrderFileTestRepository : IOrderRepository
    {
        public OrderEntry Load(string fileName)
        {
            return new OrderEntry()
            {
                OrderList = new List<Order>()
                {
                    new Order()
                    {
                        OrderNumber = "1",
                        CustomerName = "Wise",
                        State = "OH",
                        TaxRate = 6.25M,
                        ProductType = "Carpet",
                        Area = 100M,
                        CostPerSqFt = 2.25M,
                        LaborPerSqFt = 2.10M,
                        MaterialCost = 2.25M,
                        TotalLaborCost = 210.00M,
                        TotalTax = 27.19M,
                        OrderTotal = 462.19M
                    },
                    new Order()
                    {
                        OrderNumber = "2",
                        CustomerName = "Ward",
                        State = "PA",
                        TaxRate = 6.75M,
                        ProductType = "Laminate",
                        Area = 100M,
                        CostPerSqFt = 1.75M,
                        LaborPerSqFt = 2.10M,
                        MaterialCost = 175.00M,
                        TotalLaborCost = 210.00M,
                        TotalTax = 25.99M,
                        OrderTotal = 410.99M
                    },
                    new Order()
                    {
                        OrderNumber = "3",
                        CustomerName = "Johnson",
                        State = "MI",
                        TaxRate = 5.75M,
                        ProductType = "Tile",
                        Area = 100M,
                        CostPerSqFt = 3.50M,
                        LaborPerSqFt = 4.15M,
                        MaterialCost = 350.00M,
                        TotalLaborCost = 415.00M,
                        TotalTax = 49.99M,
                        OrderTotal = 814.99M
                    },
                    new Order()
                    {
                        OrderNumber = "4",
                        CustomerName = "Smith",
                        State = "IN",
                        TaxRate = 6.00M,
                        ProductType = "Wood",
                        Area = 100M,
                        CostPerSqFt = 5.15M,
                        LaborPerSqFt = 4.75M,
                        MaterialCost = 515.00M,
                        TotalLaborCost = 475.00M,
                        TotalTax = 59.40M,
                        OrderTotal = 1049.40M
                    }
                }
            };
        }

        

        public void Save(string fileName, OrderEntry orders)
        {
            return;
        }
    }
}
